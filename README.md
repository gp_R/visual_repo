# Cross-Platform for Two Projection 2D/3D Image Registration #
ITK-based visualization software cross platform for fast 2-projection 2D-3D Registration. 

## Getting Started ##
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- VTK 
- ITK v4.9 
- Qt4 
- CMake building environment.

### Installing
Make sure you installed all the prerequisites and it works well. 
Then run these commands: 
```
mkdir build 
cd build 
cmake .. 
make 
cd Application
./Application 

```
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
