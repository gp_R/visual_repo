#ifndef RESULTSWINDOW_H
#define RESULTSWINDOW_H

#include <QMainWindow>


#include "itkRescaleIntensityImageFilter.h"
#include "itkImage.h"
#include "itkImageToVTKImageFilter.h"
#include "tiffDisplay.h"
#include <QString>
#include "vtkRenderer.h"
#include <QObject>

namespace Ui {
class resultsWindow;
}


class resultsWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit resultsWindow(QWidget *parent = 0);
    ~resultsWindow();

    void testWindw();


    static const    unsigned int    Dimension = 3;
    typedef  float           InternalPixelType;
    typedef unsigned char	OutputPixelType;
    typedef itk::Image< OutputPixelType, Dimension > OutputImageType;
    typedef itk::Image< InternalPixelType, Dimension > InternalImageType;
    typedef itk::RescaleIntensityImageFilter<
            InternalImageType, OutputImageType > RescaleFilterType;
    RescaleFilterType::Pointer m_rescaler1;
    RescaleFilterType::Pointer m_rescaler2;
    
    vtkSmartPointer<vtkTIFFReader> reader1;
	vtkSmartPointer<vtkTIFFReader> reader2;
	double newRx,newRy,newRz,newTx,newTy,newTz;
	int numIteration; 
    double nccValue;

signals:

private slots:
    void renderCominedDRR();
    
    void U_updateOpacity(int opacity);
    void U_updateThreshold(int threshold);

private:
    Ui::resultsWindow *ui;

    typedef itk::Image<unsigned char, 3>  OImageType;
    typedef itk::Image<float, 3>  IImageType;

    typedef itk::ImageToVTKImageFilter<OImageType> ConnectorType;

	tiffDisplay * tiffCombined1;
	tiffDisplay * tiffCombined2;


public slots:
};

#endif // RESULTSWINDOW_H
