#ifndef UI_RESULTSWINDOW_H
#define UI_RESULTSWINDOW_H

#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtCore>
#include <QtGui>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QBoxLayout>
#include <QLine>
#include <QCheckBox>
#include <QTextBlock>
#include <QTextEdit>
#include <QVTKWidget.h>
#include <QFrame>


QT_BEGIN_NAMESPACE

class Ui_resultsWindow
{

public:

    QWidget     *centralWidget;

    QVTKWidget  *stackViewProjOne;
    QVTKWidget  *stackViewProjTwo;

    QHBoxLayout *HB_stackContainer;
    QHBoxLayout *HB_reportContainer;
    QVBoxLayout *VB_mainContainer;
    QVBoxLayout *VB_translations;
    QVBoxLayout *VB_rotations;
    QVBoxLayout *VB_information;
    QVBoxLayout *VB_xyzBox;
    QHBoxLayout *HB_trasnformations;

    QTextEdit   *TE_rotateX;
    QTextEdit   *TE_rotateY;
    QTextEdit   *TE_rotateZ;

    QTextEdit   *TE_translatX;
    QTextEdit   *TE_translatY;
    QTextEdit   *TE_translatZ;

    QTextEdit   *TE_IterNo;
    QTextEdit   *TE_NCCValue;

    QPushButton *show;
    QSlider     *SL_threshold;
    QSlider     *SL_opacity;

    void setupUi(QMainWindow *resultsWindow)
    {

        if (resultsWindow->objectName().isEmpty())
        {
            resultsWindow->setObjectName(QString::fromUtf8("resultsWindow"));
        }

//        resultsWindow->setGeometry(QRect(400,400,1100,850));
        resultsWindow->setFixedSize(1100,850);

        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(resultsWindow->sizePolicy().hasHeightForWidth());
        resultsWindow->setSizePolicy(sizePolicy);


        QLabel  *LB_title = new QLabel(" Registration Results Report ");
        LB_title->setStyleSheet("corol:#48C9B0 ; font-size:25px; border-radius: 5px, width:1100; border:1px solid  #EFF0F1");
        
        QHBoxLayout *HB_title = new QHBoxLayout();
        HB_title->addWidget(LB_title,Qt::AlignCenter);
        stackViewProjOne = new QVTKWidget();
        stackViewProjTwo = new QVTKWidget();

        SL_opacity    = new QSlider(Qt::Horizontal);
        SL_threshold  = new QSlider(Qt::Horizontal);
        SL_threshold->setMaximumWidth(200);
        SL_opacity->setMaximumWidth(200);
        SL_opacity->setRange(0,100);
        SL_opacity->setSingleStep(1);

        QLabel   *LB_threshold = new QLabel("Threshold");
        LB_threshold->setFixedSize(80,35);
        LB_threshold->setAlignment(Qt::AlignCenter);
        LB_threshold->setStyleSheet("font-size:16px;");

        QLabel   *LB_opacity= new QLabel("Opacity");
        LB_opacity->setFixedSize(80,35);
        LB_opacity->setAlignment(Qt::AlignCenter);
        LB_opacity->setStyleSheet("font-size:16px; ");


        QVBoxLayout *VB_sliderBox = new QVBoxLayout();
        QHBoxLayout *HB_thrshBox = new QHBoxLayout();
        QHBoxLayout *HB_opacBox = new QHBoxLayout();

        HB_thrshBox->addStretch(2);
        HB_thrshBox->addWidget(LB_threshold,3);
        HB_thrshBox->addWidget(SL_threshold,3);
        HB_thrshBox->addStretch(2);

        HB_opacBox->addStretch(2);
        HB_opacBox->addWidget(LB_opacity,3);
        HB_opacBox->addWidget(SL_opacity,3);
        HB_opacBox->addStretch(2);
        


//        VB_sliderBox->addLayout(HB_thrshBox,3);
        VB_sliderBox->addLayout(HB_opacBox,3);

        HB_stackContainer = new QHBoxLayout();
        HB_stackContainer->addStretch();
        HB_stackContainer->addWidget(stackViewProjOne,4);
        HB_stackContainer->addWidget(stackViewProjTwo,4);
        HB_stackContainer->addStretch();

        QLabel *LB_rotations    = new QLabel(" Translation");
        QLabel *LB_translations = new QLabel(" Rotation" );
        QLabel *LB_registInfo   = new QLabel(" Information");


        LB_rotations->setAlignment(Qt::AlignCenter);
        LB_rotations->setStyleSheet("font-size:18px; border-radius: 5px; border:solid blue; width:200; height:50");

        LB_translations->setAlignment(Qt::AlignCenter);
        LB_translations->setStyleSheet("font-size:18px; border-radius: 5px; border:solid blue; width:200; height:50");

        LB_registInfo->setAlignment(Qt::AlignCenter);
        LB_registInfo->setStyleSheet("font-size:18px; border-radius: 5px; border:solid blue; width:200; height:50");



        QLabel *LB_X  = new QLabel("X");
        LB_X->setStyleSheet("font-size:18px;vertical-align:middle; width:20px");

        QLabel *LB_Y  = new QLabel("Y");
        LB_Y->setStyleSheet("font-size:18px; vertical-align:middle; width:20px");


        QLabel *LB_Z  = new QLabel("Z");
        LB_Z->setStyleSheet("font-size:18px;vertical-align:middle;width:20px");


        VB_xyzBox     = new QVBoxLayout();
        VB_xyzBox->addWidget(LB_X);
        VB_xyzBox->addWidget(LB_Y);
        VB_xyzBox->addWidget(LB_Z);


        TE_translatX  = new QTextEdit();
        TE_translatY  = new QTextEdit();
        TE_translatZ  = new QTextEdit();

        TE_translatX->setReadOnly(true);
        TE_translatY->setReadOnly(true);
        TE_translatZ->setReadOnly(true);

        TE_translatX->setFixedSize(100,40);
        TE_translatY->setFixedSize(100,40);
        TE_translatZ->setFixedSize(100,40);


        TE_rotateX    = new QTextEdit();
        TE_rotateY    = new QTextEdit();
        TE_rotateZ    = new QTextEdit();

        TE_rotateX->setReadOnly(true);
        TE_rotateY->setReadOnly(true);
        TE_rotateZ->setReadOnly(true);

        TE_rotateX->setFixedSize(100,40);
        TE_rotateY->setFixedSize(100,40);
        TE_rotateZ->setFixedSize(100,40);


        QHBoxLayout *HB_Xtrans = new QHBoxLayout();
        QHBoxLayout *HB_Ytrans = new QHBoxLayout();
        QHBoxLayout *HB_Ztrans = new QHBoxLayout();
        QHBoxLayout *HB_labels = new QHBoxLayout();

        HB_labels->addStretch(1);
        HB_labels->addWidget(LB_translations,Qt::AlignCenter);
        HB_labels->addWidget(LB_rotations,Qt::AlignCenter);
        HB_labels->addStretch(1);

        HB_Xtrans->addWidget(LB_X,1,Qt::AlignCenter);
        HB_Xtrans->addWidget(TE_rotateX,3,Qt::AlignCenter);
        HB_Xtrans->addWidget(TE_translatX,3,Qt::AlignCenter);

        HB_Ytrans->addWidget(LB_Y,1,Qt::AlignCenter);
        HB_Ytrans->addWidget(TE_rotateY,3,Qt::AlignCenter);
        HB_Ytrans->addWidget(TE_translatY,3,Qt::AlignCenter);

        HB_Ztrans->addWidget(LB_Z,1,Qt::AlignCenter);
        HB_Ztrans->addWidget(TE_rotateZ,3,Qt::AlignCenter);
        HB_Ztrans->addWidget(TE_translatZ,3,Qt::AlignCenter);


        QVBoxLayout  *VB_transformation = new QVBoxLayout();
        VB_transformation->addLayout(HB_labels);
        VB_transformation->addLayout(HB_Xtrans);
        VB_transformation->addLayout(HB_Ytrans);
        VB_transformation->addLayout(HB_Ztrans);


        TE_IterNo     = new QTextEdit();
        TE_NCCValue   = new QTextEdit();

        TE_IterNo->setFixedSize(100,40);
        TE_NCCValue->setFixedSize(100,40);

        TE_IterNo->setReadOnly(true);
        TE_NCCValue->setReadOnly(true);


        QLabel *LB_NCC  = new  QLabel(" Best NCC");
        QLabel *LB_iter = new QLabel("Iteration");

        LB_NCC->setAlignment(Qt::AlignCenter);
        LB_NCC->setStyleSheet("font-size:16px; border-radius: 5px; border:0.5px solid blue; width:100; height:40");

        LB_iter->setAlignment(Qt::AlignCenter);
        LB_iter->setStyleSheet("font-size:16px; border-radius: 5px; border:0.5px solid blue; width:100; height:40");


        QHBoxLayout *HB_NCC = new QHBoxLayout();
        QHBoxLayout *HB_Iter= new QHBoxLayout();

        HB_NCC->addWidget(LB_NCC);
        HB_NCC->addWidget(TE_NCCValue);


        HB_Iter->addWidget(LB_iter);
        HB_Iter->addWidget(TE_IterNo);


        show  = new  QPushButton("Show");
        show->setStyleSheet("font-size:22px; border-radius: 10px; color:white; background-color:#900C3F; width:50; height:30");

        VB_information   = new QVBoxLayout();
        VB_information->addLayout(HB_NCC);
        VB_information->addLayout(HB_Iter);
        VB_information->addWidget(show);




        HB_reportContainer  = new QHBoxLayout();
        HB_reportContainer->addStretch(3);
        HB_reportContainer->addLayout(VB_transformation,3);
        HB_reportContainer->addLayout(VB_information,3);
        HB_reportContainer->addStretch(3);

        QFrame* line1 = new QFrame();
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        QFrame* line2 = new QFrame();
        line2->setFrameShape(QFrame::HLine);
        line2->setFrameShadow(QFrame::Sunken);

        QFrame* line3 = new QFrame();
        line3->setFrameShape(QFrame::HLine);
        line3->setFrameShadow(QFrame::Sunken);


        VB_mainContainer  = new QVBoxLayout();
        VB_mainContainer->addLayout(HB_title);
        VB_mainContainer->addLayout(HB_stackContainer,6);
        VB_mainContainer->addWidget(line1);
        VB_mainContainer->addLayout(VB_sliderBox);
        VB_mainContainer->addWidget(line2);
        VB_mainContainer->addLayout(HB_reportContainer,2);
        VB_mainContainer->addWidget(line3);

        centralWidget = new QWidget(resultsWindow);

        centralWidget->setLayout(VB_mainContainer);

        resultsWindow->setCentralWidget(centralWidget);

        retranslateUi(resultsWindow);

    }
    void retranslateUi(QMainWindow *resultsWindow)
       {
           resultsWindow -> setWindowTitle(QApplication::translate("resultsWindow","Registration Resluts Window", 0, QApplication::UnicodeUTF8));

       }




};


namespace Ui {
      class resultsWindow : public Ui_resultsWindow {};
  }
QT_END_NAMESPACE// namespace Ui
#endif // UI_RESULTSWINDOW_H
