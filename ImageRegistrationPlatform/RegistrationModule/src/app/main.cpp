#include "RegistrationModule.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    RegistrationModule w;
    w.show();

    return app.exec();
}
