#include "resultswindow.h"
#include "ui_resultswindow.h"

using namespace std;

resultsWindow::resultsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::resultsWindow)
{

    ui->setupUi(this);
    //ui -> SL_threshold -> readOnly (false);
	//ui -> SL_opacity -> readOnly (false);
    connect(ui->show, SIGNAL(clicked()), this, SLOT(renderCominedDRR()));
	connect(this->ui->SL_threshold,  SIGNAL(sliderMoved(int)),     this, SLOT(U_updateThreshold(int)));
    connect(this->ui->SL_opacity  ,  SIGNAL(sliderMoved(int)),     this, SLOT(U_updateOpacity(int)));
	 ui-> TE_rotateX -> setText("");
	ui-> TE_rotateY -> setText("");
	ui-> TE_rotateZ -> setText(""); 
	ui-> TE_translatX -> setText("");
	ui-> TE_translatY -> setText("");
	ui-> TE_translatZ -> setText("");
	ui-> TE_NCCValue -> setText(""); 
	ui->  TE_IterNo -> setText("");
}
resultsWindow::~resultsWindow()
{
    delete ui;
   
}


void resultsWindow::testWindw(){

    qDebug()<< " Test is running successfully";
}


void resultsWindow::renderCominedDRR(){

   tiffCombined1 = new tiffDisplay(ui->stackViewProjOne->GetRenderWindow());
   tiffCombined2 = new tiffDisplay(ui->stackViewProjTwo->GetRenderWindow());
   
   
       if(m_rescaler1  && m_rescaler2 ){  
			 tiffCombined1 ->RenderTiffWithRefence(m_rescaler1,reader1);	
			 tiffCombined2 ->RenderTiffWithRefence(m_rescaler2,reader2);
			 ui-> TE_rotateX -> setText(QString::number (newRx));
			 ui-> TE_rotateY -> setText(QString::number (newRy));
			 ui-> TE_rotateZ -> setText(QString::number (newRz));
			 ui-> TE_translatX -> setText(QString::number (newTx));
		     ui-> TE_translatY -> setText(QString::number (newTy));
		     ui-> TE_translatZ -> setText(QString::number (newTz));
			      
			 ui-> TE_NCCValue -> setText(QString::number (nccValue));
			 ui-> TE_IterNo -> setText(QString::number (numIteration));
			
			ui -> SL_threshold -> setEnabled(1);
			ui -> SL_threshold -> setValue(50);
			ui -> SL_opacity -> setEnabled(1);
			ui -> SL_opacity -> setValue(50);	      
		}
     else
          QMessageBox::critical(
              this,
              tr("Results"),
              tr("No Input? No Output, that's it!") );


}
void resultsWindow::U_updateThreshold(int threshold){
/* No Implementation */ 

}

void resultsWindow::U_updateOpacity(int opacity){
	
	const int overStep = 100;
	double uOpacity = double (opacity % overStep) / double(overStep);
	if(m_rescaler1  && m_rescaler2 ){
		
		tiffCombined1 ->  updateOpacity(uOpacity);
		tiffCombined2 ->  updateOpacity(uOpacity);
	}

}
