#ifndef REGISTRATIONMODULE_H
#define REGISTRATIONMODULE_H

#include<QMainWindow>
#include <list>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkInteractorStyleImage.h>
#include <vtkRenderWindow.h>

#include <vtkImageData.h>
#include "Render3D.h"
#include <sstream>
#include "QVTKWidget.h"
#include <QVBoxLayout>
#include <QSpinBox>
#include <QLabel>
#include <QTextStream>
#include <QFileDialog>
#include <QThread>	

#include <iostream>
#include "tiffDisplay.h"
#include <vtkMarchingCubes.h>
#include <vtkActor.h>
#include <vtkStripper.h>
#include <vtkPolyDataMapper.h>

#include "Logger.h"
#include "twoProj2D3DReg.h"
#include "generateDRR.h"

#include "itkGPUImage.h"
#include "itkGPUKernelManager.h"
#include "itkGPUContextManager.h"

#include "resultswindow.h"

#include "VisualizationModule.h"

#pragma GCC visibility push(hidden)

namespace Ui {
class RegistrationModule;
}

class RegistrationModule : public QMainWindow
{
    Q_OBJECT

    public:
       explicit RegistrationModule(QWidget *parent = 0);
       ~RegistrationModule();
		
		
    private slots:

	   //void QT_PlainTextEdit_appendPlainText(QString line);
	   //void QT_PlainTextEdit_SetText(QString line);	
       void RenderTiffA1();
       void RenderTiffA2();
       void RenderCTVolume();
       //  void updateWidget(char *fileName);
       void _2D3DTwoProjRegistration();

       void _generateDRR();
       void saveDRR();
       //void LogString(char* LOG);
       void setIso();
       void setCenter1();
       void setCenter2();
       void setRes1();
       void setRes2();
       void setSCD();
       void setThresh();
	   void setDRRSize();

	   void logResults();
       void append2Logger(double );

       void result();
       
    signals:
			void updateText(QString text);
			void signalProgress(int value);
			void started();
            void PERFORM();   //A signal to alter the thread to start
            void FINISHED();   //A signal to alter the thread to finish
            void SHAREDIR(QString volumeDir );
//            void SHARERENDERER(vtkRenderer *renderer );

	
    private:
        Ui::RegistrationModule *ui;

		/**
		* @brief registraitonThread_
		*/
		
        QThread *registraitonThread_;
        QThread *visualizttoinThread_;
        QThread *CTrenderThreadd;

        /**
         * @brief renderer_
         */
        Render3D* renderer_ ;

		/**
		* @brief volumeDir_
		*/
		QString volumeDir_;

		/**
		* @brief fileInput_1
		*/
		
		QString fileInput_1;

		/**
		* @brief fileInput_2
		*/
		QString fileInput_2;

		/**
		* @brief fileOutput_1
		*/
        std::string fileOutput_1;

        /**
		* @brief fileInput_2
		*/
        std::string fileOutput_2;


		/**
		* @brief interactor_
		*/
		vtkSmartPointer<vtkRenderWindowInteractor> interactor_;
	
		vtkSmartPointer<vtkInteractorStyleImage> style_ ; 


        double projAngle1;
      	double projAngle2;
        double rx;
        double ry;
      	double rz;

      	double tx;
      	double ty;
      	double tz;
      	
      	double cx;
      	double cy;
      	double cz;

      	double scd; // Source to isocenter distance

        double image1resX;
      	double image1resY;
      	double image2resX;
      	double image2resY;

      	double threshold;

        double image1centerX;
        double image1centerY;
        double image2centerX;
        double image2centerY;

        bool stateIso,stateCenter1,stateCenter2,stateRes1, stateRes2, stateThres, stateSCD, stateSize;
        vtkSmartPointer<vtkRenderer> _qvtkRenderer;
//        Render3D *renderer_;
        
        tiffDisplay *x_ray1;
        tiffDisplay *x_ray2;


        vtkImageData* data_;

        bool registrationflag;
        
        double newRx;
        double newRy;
      	double newRz;

      	double newTx;
      	double newTy;
      	double newTz;
      	int numIteration;
      	double nccValue;
		static const    unsigned int    Dimension = 3;
		typedef  float           InternalPixelType;
		typedef  short           PixelType3D;
		typedef itk::GPUImage<float, 2> ItkImage1f;

		typedef itk::Image< PixelType3D, Dimension > ImageType3D;

		typedef unsigned char	OutputPixelType;
		typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

		typedef itk::Image< InternalPixelType, Dimension > InternalImageType;

		// Rescale the intensity of the projection images to 0-255 for output.
		typedef itk::RescaleIntensityImageFilter<
				InternalImageType, OutputImageType > RescaleFilterType;
		RescaleFilterType::Pointer rescaler1;
		RescaleFilterType::Pointer rescaler2;
		

        void updateRenderWindow();


        resultsWindow *resultsWindowPOPUP;

        //virtual bool notify(QObject* receiver, QEvent* event);
	public:
		twoProj2D3DReg *performtwoProj2D3DReg;
		generateDRR *performDRR;
		
		

        
	
		
};
#pragma GCC visibility pop
#endif 
