#include "RegistrationModule.h"
#include "ui_RegistrationModule.h"
#include "toLogger.h"
#include <QtCore>
#include <QObject>
#include <QThread>
using namespace std;

RegistrationModule::RegistrationModule(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RegistrationModule)
{

    ui->setupUi(this);
    updateRenderWindow();

    stateIso = false;
    stateCenter1 = false;
    stateCenter1 = false;
    stateRes1 = false;
    stateRes2 = false;
    stateThres = false;
    stateSCD = false;
    stateSize = false;
	registrationflag = false;
    ui->iso1-> setEnabled(false);
    ui->iso2-> setEnabled(false);
    ui->iso3-> setEnabled(false);
	ui -> resampleratio -> setEnabled(false);
    ui->cx1-> setEnabled(false);
    ui->cy1-> setEnabled(false);
    ui->cx2-> setEnabled(false);
    ui->cy2-> setEnabled(false);

    ui-> res1 -> setEnabled(false);
    ui-> res2 -> setEnabled(false);
    ui-> res3 -> setEnabled(false);
    ui-> res4 -> setEnabled(false);

    ui->threshold-> setEnabled(false);
    ui->scd-> setEnabled(false);
    performDRR = new generateDRR();

    //---- Setting up threading -------------//

    registraitonThread_ = new QThread;

    performtwoProj2D3DReg = new twoProj2D3DReg();
    performtwoProj2D3DReg -> moveToThread(registraitonThread_);

    connect(registraitonThread_, SIGNAL(started()), performtwoProj2D3DReg, SLOT(setPerformingRegistrationON()));
    connect(performtwoProj2D3DReg, SIGNAL(FINISHED()), registraitonThread_, SLOT(quit()));   //signal from registration process
    connect(registraitonThread_, SIGNAL(finished()), this, SLOT(logResults()));   //log and render are seperated to prevent race conditions.



//    visualizttoinThread_ = new QThread;

//    performRendering_VM    = new VisualizationModule();
//    performRendering_VM -> moveToThread(visualizttoinThread_);

//    connect(visualizttoinThread_, SIGNAL(started()), performRendering_VM, SLOT(SetRenderingStartON()));
//    connect(performRendering_VM, SIGNAL(FINISHED()), visualizttoinThread_, SLOT(quit()));   //signal from registration process

    //------------- END --------------------//

    resultsWindowPOPUP = new resultsWindow() ;
    connect(ui->_2D3DRegRes, SIGNAL(clicked()),resultsWindowPOPUP,SLOT( show() ));

    connect(ui->inputImage2D1, SIGNAL(clicked()),this,SLOT( RenderTiffA1() ));
    connect(ui->inputImage2D2, SIGNAL(clicked()),this,SLOT( RenderTiffA2() ));
    connect(ui->inputCTVolume, SIGNAL(clicked()),this,SLOT( RenderCTVolume() ));
    connect(ui->_2D3DReg, SIGNAL(clicked()), this, SLOT( _2D3DTwoProjRegistration() ));

    connect(ui->generateDRR, SIGNAL(clicked()), this, SLOT( _generateDRR() ));

    connect(ui->saveDRR, SIGNAL(clicked()), this, SLOT( saveDRR() ));
    connect(ui->checkIso,SIGNAL(stateChanged(int)),this,SLOT(setIso() ));
    connect(ui->checkCenter1 ,SIGNAL(stateChanged(int)),this,SLOT(setCenter1() ));
    connect(ui->checkCenter2 ,SIGNAL(stateChanged(int)),this,SLOT(setCenter2() ));
    connect(ui->checkRes1 ,SIGNAL(stateChanged(int)),this,SLOT(setRes1() ));
    connect(ui->checkRes2 ,SIGNAL(stateChanged(int)),this,SLOT(setRes2() ));
    connect(ui->checkSCD, SIGNAL(stateChanged(int)),this, SLOT(setSCD()));
    connect(ui->checkThres, SIGNAL(stateChanged(int)),this, SLOT(setThresh()));
    connect(ui->Resample, SIGNAL(currentIndexChanged(QString)), this, SLOT(setDRRSize()));
    connect(this, SIGNAL(signalProgress(int) ), ui->progressbar, SLOT( setValue(int) ) );
//    connect(ui->_2D3DRegRes, SIGNAL(clicked()), this, SLOT( result() ));


}

RegistrationModule::~RegistrationModule()
{
    delete ui;
}


void RegistrationModule::append2Logger(double value){
     qDebug() << "Signal is received from registration thread, trying to append data" << value;
     ui-> LoggerDisplay -> append ("Number of Iteration =  "+ QString::number(value));

}

void RegistrationModule::RenderTiffA1()
{
         fileInput_1 = QFileDialog::getOpenFileName(this,tr("Open Image"),QDir::currentPath(),
                                                                tr("Image Files (*.tif *.jpg)"));
        if(!fileInput_1.isEmpty()&& !fileInput_1.isNull())
        {
		 //interactor_ = vtkSmartPointer<vtkRenderWindowInteractor>::New();
		// style_	  = vtkSmartPointer<vtkInteractorStyleImage>::New();
		 vtkRenderWindow* renwin =  ui -> regIn1 -> GetRenderWindow();
		// interactor_ = renwin->GetInteractor();
		// interactor_ -> SetInteractorStyle(style_);
		 
         x_ray1 = new tiffDisplay(renwin);
         x_ray1->loadData((char*) fileInput_1.toStdString().c_str() );
         x_ray1->RenderTiff();
         }
        else
            QMessageBox::information(
            this,
            tr("Select image "),tr("No image Selected!"));

}

void RegistrationModule::RenderTiffA2()
{
         fileInput_2 = QFileDialog::getOpenFileName(this,tr("Open Image"),QDir::currentPath(),
                                                                tr("Image Files (*.tif *.jpg)"));
        if(!fileInput_2.isEmpty()&& !fileInput_2.isNull())
        {
		// interactor_ = vtkSmartPointer<vtkRenderWindowInteractor>::New();
		// style_	  = vtkSmartPointer<vtkInteractorStyleImage>::New();
		 vtkRenderWindow* renwin =  ui -> regIn2 -> GetRenderWindow();
		//interactor_ = renwin->GetInteractor();
		 //interactor_ -> SetInteractorStyle(style_);
         x_ray2 = new tiffDisplay(renwin);
         x_ray2->loadData((char*) fileInput_2.toStdString().c_str() );
         x_ray2->RenderTiff();
        }

        else
            QMessageBox::information(
            this,
            tr("Select image "),tr("No image Selected!"));

}

void RegistrationModule::RenderCTVolume()
{
            CTrenderThreadd = new QThread;

            renderer_ = new Render3D(ui -> CTIn -> GetRenderWindow());
            renderer_ -> moveToThread(CTrenderThreadd);

            connect(CTrenderThreadd, SIGNAL(started()), renderer_, SLOT(rayCastingRendering()));
            connect(renderer_, SIGNAL(FINISHED()), renderer_, SLOT(updateRenderer()));   //signal from registration process
            connect(renderer_, SIGNAL(FINISHED()), CTrenderThreadd, SLOT(quit()));   //signal from registration process
            connect(renderer_, SIGNAL(FINISHED()), this, SLOT(shareRenderer()));   //signal from registration process

          // Dicom Directory
          volumeDir_ = QFileDialog::getExistingDirectory(this, tr("Open Directory"),QDir::currentPath(),
                                    QFileDialog::ShowDirsOnly |QFileDialog::DontResolveSymlinks);

          // img file
          // volumeDir_ = QFileDialog::getOpenFileName(this,tr("Open Image"),QDir::currentPath(),
          //                                                     tr("Image Files (*.img *.hdr)"));

          //Start thread to perform registation

          if(!volumeDir_.isEmpty()&& !volumeDir_.isNull())
        {
           emit SHAREDIR(volumeDir_);

          renderer_ ->loadData((char*) volumeDir_.toStdString().c_str());
          renderer_->rescaleData(renderer_->getScale(),
                                 renderer_->getShift());
           //  Marching Cube
          // renderer_->cubeMarchingExtraction(1200, renderer_->
          // getShifter()->GetOutput());
          renderer_->setShifter(renderer_->getShifter());

          renderer_->setInputData(renderer_->
                                            getShifter()->GetOutput());

//          renderer_->rayCastingRendering();
          qDebug()<<" Main thread: " << QThread::currentThread() ;
          qDebug()<<" Main thread qAPP: " << qApp->thread() ;

          CTrenderThreadd->start();


         }

         else
            QMessageBox::information(
            this,
            tr("Open Directory"),
            tr("No Directory Selected!") );

}


void RegistrationModule :: setIso(){
    bool state  = ui -> checkIso ->  isChecked();
    if (state == false){
        stateIso = false;
        ui->iso1-> setEnabled(false);
        ui->iso2-> setEnabled(false);
        ui->iso3-> setEnabled(false);
        // ui->iso1-> setValue(0.0);
        // ui->iso2-> setValue(0.0);
       //  ui->iso3-> setValue(0.0);
    }
    if (state == true){
        stateIso = true;
        ui->iso1-> setEnabled(true);
        ui->iso2-> setEnabled(true);
        ui->iso3-> setEnabled(true);
    }

}

void RegistrationModule :: setCenter1(){
    bool state  = ui -> checkCenter1 ->  isChecked();
    if (state == false){
        stateCenter1 = false;
        ui->cx1-> setEnabled(false);
        ui->cy1-> setEnabled(false);
		ui->cx1-> setValue(0.0);
        ui->cy1-> setValue(0.0);
    }
    if (state == true){
        stateCenter1 = true;
        ui->cx1-> setEnabled(true);
        ui->cy1-> setEnabled(true);

    }

}

void RegistrationModule :: setCenter2(){
    bool state  = ui -> checkCenter2 ->  isChecked();
    if (state == false){
        stateCenter2 = false;
        ui->cx2-> setEnabled(false);
        ui->cy2-> setEnabled(false);
        ui->cx2-> setValue(0.0);
        ui->cy2-> setValue(0.0);

    }
    if (state == true){
        stateCenter2 = true;
        ui->cx2-> setEnabled(true);
        ui->cy2-> setEnabled(true);

    }

}

void RegistrationModule :: setRes1(){
    bool state  = ui -> checkRes1 ->  isChecked();
    if (state == false){
        stateRes1 = false;
        ui->res1-> setEnabled(false);
        ui->res2-> setEnabled(false);
        ui -> res1 -> setValue(1.00);
        ui -> res2 -> setValue(1.00);


    }
    if (state == true){
        stateRes1 = true;
        ui->res1-> setEnabled(true);
        ui->res2-> setEnabled(true);

    }

}

void RegistrationModule :: setRes2(){
    bool state  = ui -> checkRes2 ->  isChecked();
    if (state == false){
        stateRes2 = false;
        ui->res3-> setEnabled(false);
        ui->res4-> setEnabled(false);
        ui -> res3 -> setValue(1.00);
        ui -> res4 -> setValue(1.00);

    }
    if (state == true){
        stateRes2 = true;
        ui->res3-> setEnabled(true);
        ui->res4-> setEnabled(true);

    }

}

void RegistrationModule :: setThresh(){
    bool state  = ui -> checkThres ->  isChecked();
    if (state == false){
        stateThres = false;
        ui->threshold-> setEnabled(false);
        ui ->threshold-> setValue(0.0);
    }
    if (state == true){
        stateThres = true;
        ui->threshold-> setEnabled(true);

    }

}

void RegistrationModule :: setSCD(){
    bool state  = ui -> checkSCD ->  isChecked();
    if (state == false){
        stateSCD = false;
        ui->scd-> setEnabled(false);
        ui ->scd-> setValue(1000.0);

    }
    if (state == true){
        stateSCD = true;
        ui->scd-> setEnabled(true);

    }

}

void RegistrationModule :: setDRRSize(){

    QString size_stat = ui-> Resample ->currentText();
    if (size_stat == "Full DRR")
    {
        LOG_DEBUG("FULL CHECKED");
        
        stateSize = false;
		ui -> resampleratio -> setEnabled(false);

    }
    if (size_stat == "Downsampled DRR")
    {
        LOG_DEBUG("Downsampled CHECKED");
        stateSize = true;
		ui -> resampleratio -> setEnabled(true);
        }
}

void RegistrationModule::_2D3DTwoProjRegistration(){

    //check if registration is already running or not

    if(registraitonThread_->isRunning()){
        QMessageBox::information(
            this,
            tr("Running Process"),
            tr("Registration process is already running, please wait! ") );
        return;
    }


    //emit updateText("Registration variables are beem set ..");
    //emit updateText(" Performing Registration  ...");
    ui -> LoggerDisplay -> setText("");
    projAngle1 = ui -> proj1 -> value();
    projAngle2 = ui -> proj2 -> value();

    //ui-> LoggerDisplay -> append("Startd .. ");
    tx = ui -> tx -> value();
    ty = ui -> ty -> value();
    tz = ui -> tz -> value();

    rx = ui -> rx -> value();
    ry = ui -> ry -> value();
    rz = ui -> rz -> value();
	
    image1resX = ui -> res1 -> value();
    image1resY = ui -> res2 -> value();
    image2resX = ui -> res3 -> value();
    image2resY = ui -> res4 -> value();

    threshold = ui -> threshold -> value();

    scd = ui -> scd -> value();

    cx = ui -> iso1 -> value();
    cy = ui -> iso2 -> value();
    cz = ui -> iso3 -> value();

    image1centerX = ui -> cx1 -> value();
    image1centerY = ui -> cy1 -> value();
    image2centerX = ui -> cx2 -> value();
    image2centerY = ui -> cy2 -> value();

    emit signalProgress(5);

   std::cout << QThread::currentThread();
   
		

        if ( (!volumeDir_.isEmpty()&& !volumeDir_.isNull()) &&
			 (!fileInput_1.isEmpty()&& !fileInput_1.isNull()) &&
			 (!fileInput_2.isEmpty()&& !fileInput_2.isNull()) )
                {
                    LOG_DEBUG(" PERFORMING DRR RENDERING ");
//                    performtwoProj2D3DReg -> performRegistration(projAngle1,projAngle2,rx, ry, rz,
//                                                        tx, ty, tz, cx, cy, cz, scd, threshold,
//                                                        image1resX, image1resY,  image2resX,image2resY,
//                                                        image1centerX, image1centerY, image2centerX, image2centerY,
//                                                        volumeDir_,fileInput_1,fileInput_2,
//                                                        stateIso,stateCenter1 ,stateCenter2,stateRes1,stateRes2,
					performtwoProj2D3DReg -> setOutSpacingSize( ui -> resampleratio -> value());
                    performtwoProj2D3DReg -> setRegistrationParameters(projAngle1,projAngle2,rx, ry, rz,
                                                                   tx, ty, tz, cx, cy, cz, scd, threshold,
                                                                   image1resX, image1resY,  image2resX,image2resY,
                                                                   image1centerX, image1centerY, image2centerX, image2centerY,
                                                                   volumeDir_,fileInput_1,fileInput_2,
                                                                   stateIso,stateCenter1 ,stateCenter2,stateRes1,stateRes2,
                                                                   stateThres, stateSCD,stateSize);

                    LOG_DEBUG(" DONE SETTING ");
                    // performtwoProj2D3DReg->setPerformingRegistrationON();
                    qDebug() << " Before starting:: call from main thread  :" << QThread::currentThreadId();

                   //double check and mayy used to override the running process (=re-run)
                   if(registraitonThread_->isRunning()){
                       LOG_DEBUG("Thread was running");
                        registraitonThread_->quit();
                        registraitonThread_->wait(100);
                        registraitonThread_->start();
                   }
                   else{
                        LOG_DEBUG("Thread to be started");
                        registraitonThread_->start();

                   }


                }



        else {

            QMessageBox::information(
            this,
            tr("Select your inputs"),
            tr("Registration process is requiring CT volume and a two input 2D images ") );
        }


    emit signalProgress(80);
    // Ray Casting


    }


void RegistrationModule::logResults(){

    qDebug() << "Main thread check point :  " << QThread::currentThreadId() ;

    if(registraitonThread_->isRunning()){
           qDebug() << "Called from thread :  " << QThread::currentThread() ;
           registraitonThread_->terminate();
           registraitonThread_->wait(500);


    }

    rescaler1 = performtwoProj2D3DReg->getrescaler1();
    rescaler2 = performtwoProj2D3DReg->getrescaler2();
    rescaler1->Update();
    rescaler2->Update();
    tiffDisplay *tiffO1 = new tiffDisplay(ui->regOut1 -> GetRenderWindow());
    tiffO1->RenderTiffData(rescaler1);
    tiffDisplay *tiffO2 = new tiffDisplay(ui->regOut2 -> GetRenderWindow());
    tiffO2->RenderTiffData(rescaler2);

   

    qDebug() << "Logger is called from thread :  " << QThread::currentThreadId() ;
	 
	 newRx = performtwoProj2D3DReg -> getRotationAlongX();
     newRy = performtwoProj2D3DReg -> getRotationAlongY();
     newRz = performtwoProj2D3DReg -> getRotationAlongZ();

     newTx = performtwoProj2D3DReg -> getTranslationAlongX();
     newTy = performtwoProj2D3DReg -> getTranslationAlongY();
     newTz = performtwoProj2D3DReg -> getTranslationAlongZ();
     nccValue = performtwoProj2D3DReg -> getBestValue();
	 numIteration  = performtwoProj2D3DReg -> getNumberOfIterations() +1;
     ui-> LoggerDisplay -> append ("Rotation around X =  " + QString::number (newRx));
     ui-> LoggerDisplay -> append ("Rotation around Y = " + QString::number  (newRy));
     ui-> LoggerDisplay -> append ("Rotation around Z = " + QString::number  (newRz));
     ui-> LoggerDisplay -> append ("Translation in X = " +  QString::number (newTx));
     ui-> LoggerDisplay -> append ("Translation in Y = " +  QString::number (newTy));
     ui-> LoggerDisplay -> append ("Translation in Z = " +  QString::number (newTz));
     ui-> LoggerDisplay -> append ("Number of Iteration =  "+ QString::number(numIteration));
     ui-> LoggerDisplay-> append  ("Best NCC Value = " + QString::number(nccValue ));

     emit signalProgress(100);

     QMessageBox::information(
     this,
     tr("Registration Process"),
     tr("Registration is done successfully") );
     registrationflag = true; 
	 this -> result();

}
void  RegistrationModule::result(){

	if(registrationflag){
		// Inverse 
		double invRotationAlongX = -newRx;
		double invRotationAlongY = -newRy;
		double invRotationAlongZ = -newRz;
		double invTranslationAlongX = -newTx;
		double invTranslationAlongY = -newTy;
		double invTranslationAlongZ = -newTz;
		generateDRR * performDRRFinal = new generateDRR();
		
		performDRRFinal -> renderDRR(projAngle1,projAngle2,invRotationAlongX, invRotationAlongY, invRotationAlongZ,
                                     invTranslationAlongX, invTranslationAlongY, invTranslationAlongX, cx, cy, cz, scd, threshold,
                                     image1resX, image1resY,  image2resX,image2resY,
                                     image1centerX, image1centerY, image2centerX, image2centerY,
                                     volumeDir_, stateIso,stateCenter1 ,stateCenter2,stateRes1,stateRes2,
                                     stateThres, stateSCD,stateSize);
	
		rescaler1 = performDRRFinal->getrescaler1();
	    rescaler2 = performDRRFinal->getrescaler2();
	    rescaler1->Update();
	    rescaler2->Update();
	    
	    resultsWindowPOPUP->m_rescaler1 = rescaler1;
		resultsWindowPOPUP->m_rescaler2 = rescaler2;
		resultsWindowPOPUP-> reader1 = x_ray1 -> getReader();
		resultsWindowPOPUP-> reader2 = x_ray2 -> getReader();
		resultsWindowPOPUP-> newRx = newRx;
	   	resultsWindowPOPUP-> newRy = newRy;
	   	resultsWindowPOPUP-> newRz = newRz;
	   	resultsWindowPOPUP-> newTx = newTx;
	   	resultsWindowPOPUP-> newTy = newTy;
	   	resultsWindowPOPUP-> newTz = newTz;
		resultsWindowPOPUP -> numIteration = numIteration;
		resultsWindowPOPUP -> nccValue = nccValue;
		


	}
}
void RegistrationModule::_generateDRR(){

//emit updateText("Registration variables are beem set ..");
    //emit updateText(" Performing Registration  ...");
    //ui -> LoggerDisplay -> setText("");
    projAngle1 = ui -> proj1 -> value();
    projAngle2 = ui -> proj2 -> value();

    //ui-> LoggerDisplay -> append("Startd .. ");
    tx = ui -> tx -> value();
    ty = ui -> ty -> value();
    tz = ui -> tz -> value();

    rx = ui -> rx -> value();
    ry = ui -> ry -> value();
    rz = ui -> rz -> value();

    image1resX = ui -> res1 -> value();
    image1resY = ui -> res2 -> value();
    image2resX = ui -> res3 -> value();
    image2resY = ui -> res4 -> value();

    threshold = ui -> threshold -> value();

    scd = ui -> scd -> value();

    cx = ui -> iso1 -> value();
    cy = ui -> iso2 -> value();
    cz = ui -> iso3 -> value();

    image1centerX = ui -> cx1 -> value();
    image1centerY = ui -> cy1 -> value();
    image2centerX = ui -> cx2 -> value();
    image2centerY = ui -> cy2 -> value();

    emit signalProgress(5);

    //int x = ui -> Resample ->
    if ( !volumeDir_.isEmpty()&& !volumeDir_.isNull())
        {
                    LOG_DEBUG(" PERFORMING DRR RENDERING ");
                    performDRR -> setOutSpacingSize( ui -> resampleratio -> value());
                    performDRR -> renderDRR(projAngle1,projAngle2,rx, ry, rz,
                                     tx, ty, tz, cx, cy, cz, scd, threshold,
                                     image1resX, image1resY,  image2resX,image2resY,
                                     image1centerX, image1centerY, image2centerX, image2centerY,
                                     volumeDir_, stateIso,stateCenter1 ,stateCenter2,stateRes1,stateRes2,
                                     stateThres, stateSCD,stateSize);

            }




    else  {


            QMessageBox::information(
            this,
            tr("Select CT DICOM Directory"),
            tr("DRR generation process is requiring CT volume ") );
    }
    emit signalProgress(100);
    rescaler1 = performDRR->getrescaler1();
    rescaler2 = performDRR->getrescaler2();
    rescaler1->Update();
    rescaler2->Update();
    tiffDisplay *tiffO1 = new tiffDisplay(ui->regOut1 -> GetRenderWindow());
    tiffO1->RenderTiffData(rescaler1);
    tiffDisplay *tiffO2 = new tiffDisplay(ui->regOut2 -> GetRenderWindow());
    tiffO2->RenderTiffData(rescaler2);


}

void RegistrationModule::saveDRR(){

     fileOutput_1 = std::string("Image2D1_DRR.tif");
     fileOutput_2 = std::string("Image2D2_DRR.tif");

     typedef itk::ImageFileWriter< OutputImageType >  WriterType;

     WriterType::Pointer writer1 = WriterType::New();
     WriterType::Pointer writer2 = WriterType::New();

     writer1->SetFileName(fileOutput_1);
     writer1->SetInput( rescaler1->GetOutput() );
     LOG_DEBUG("Writing DRR Image 1");
     try {
        std::cout << "Writing image: " << fileOutput_1 << std::endl;
        writer1->Update();
     }
     catch( itk::ExceptionObject & err ) {

        std::cerr << "ERROR: ExceptionObject caught !" << std::endl;
        std::cerr << err << std::endl;
     }

     writer2->SetFileName(fileOutput_2);
     writer2->SetInput( rescaler2->GetOutput() );

     LOG_DEBUG("Writing DRR Image 2 ");
     try {
        std::cout << "Writing image: " << fileOutput_2 << std::endl;
        writer2->Update();
     }
     catch( itk::ExceptionObject & err ) {

        std::cerr << "ERROR: ExceptionObject caught !" << std::endl;
        std::cerr << err << std::endl;
     }
}

void RegistrationModule::updateRenderWindow(){


        vtkRenderWindow *wind   = vtkRenderWindow::New();
        ui->regOut1->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->regOut2->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->regIn1->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->regIn2->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->CTIn->SetRenderWindow(wind);

        wind->Delete();
        wind = NULL;


}


