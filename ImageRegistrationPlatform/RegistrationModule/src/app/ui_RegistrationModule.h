#ifndef UI_REGISTRATIONMODULE_H
#define UI_REGISTRATIONMODULE_H

#include <QtCore>
#include <QtGui>
#include "QVTKWidget.h"
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QWidget>
#include <QtGui/QCheckBox>
#include <QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QGroupBox>
#include <QtGui/QDoubleSpinBox>
#include <QTextEdit>
#include <QSize>
#include <QTabWidget>
#include <QSplitter>
#include <QFrame>
#include <QProgressBar>
#include <QComboBox>

QT_BEGIN_NAMESPACE

class Ui_RegistrationModule
{

public:
        QWidget* centralWidget;
        QHBoxLayout* mainHBox;
        QVBoxLayout* mainlayout;
        QVBoxLayout* Proj;
        QDoubleSpinBox* proj1;
        QDoubleSpinBox* proj2;
        QHBoxLayout* projAngle1;
        QHBoxLayout* projAngle2;
        QVBoxLayout* Rot;
        QDoubleSpinBox* rx;
        QDoubleSpinBox* ry;
        QDoubleSpinBox* rz;
        QVBoxLayout* Trans;
        QDoubleSpinBox* tx;
        QDoubleSpinBox* ty;
        QDoubleSpinBox* tz;
        QHBoxLayout* RotTransF;
        QVBoxLayout* res;
        QDoubleSpinBox* res1;
        QDoubleSpinBox* res2;
        QDoubleSpinBox* res3;
        QDoubleSpinBox* res4;
		QVBoxLayout * res1L;
        QVBoxLayout * res2L;
        QHBoxLayout * BoxH;
        
        
        QHBoxLayout* centralX;
        QDoubleSpinBox* cx1;
        QDoubleSpinBox* cy1;
        QDoubleSpinBox* cx2;
        QDoubleSpinBox* cy2;
        
        QVBoxLayout * center1;
        QVBoxLayout * center2;
        QHBoxLayout * BoxH3;

        QDoubleSpinBox* threshold;
        QDoubleSpinBox* scd;

        QHBoxLayout * threshold_scd;

        QVBoxLayout *thresholdWithL;
        QVBoxLayout *scdWithL;
		QVBoxLayout *BoxVtc;


        QDoubleSpinBox* iso1;
        QDoubleSpinBox* iso2;
        QDoubleSpinBox* iso3;

        QVBoxLayout* iso;
        QHBoxLayout* iso1L;
        QHBoxLayout* iso2L;
        QHBoxLayout* iso3L;

        //Load Data button//
        QHBoxLayout *LoadH;
        QVBoxLayout *LoadV2;
        QVBoxLayout *loadButtons;
		QVBoxLayout *LoadV ;
		QVBoxLayout *performSave;
        QPushButton *inputImage2D1;
        QPushButton *inputImage2D2;
        QPushButton *inputCTVolume;
        QPushButton *_2D3DReg;
        QPushButton *_2D3DRegRes;
        QPushButton *saveDRR;
        QPushButton *generateDRR;
        QVBoxLayout * regAndresults;
        QVTKWidget * regOut1;
        QVTKWidget * regOut2;
        QVTKWidget * regIn1;
        QVTKWidget * regIn2;
        QVTKWidget * CTIn;
        QHBoxLayout *outputH;
        QVBoxLayout *outputV1;
        QVBoxLayout *outputV2;
        QHBoxLayout *HBox6;
        QVBoxLayout *V_outPut;
        QWidget *controlsRestrictorWidget1 ;
        QWidget *controlsRestrictorWidget2 ;
        QWidget *controlsRestrictorWidget3 ; 
        QVBoxLayout *V_outPut1;
        QHBoxLayout *corr_tmatrix;
        QTextEdit *LoggerDisplay;
        QComboBox *Resample;

        QProgressBar   *progressbar;

        QCheckBox *checkIso;
        QCheckBox *checkCenter1;
        QCheckBox *checkCenter2;
        QHBoxLayout *checkIsoL;
        QCheckBox *	checkRes1;
        QCheckBox *	checkRes2;
        QCheckBox *checkThres;
        QCheckBox *checkSCD;
        QSplitter  *mainSplitter;
		QDoubleSpinBox *resampleratio;
        void setupUi(QMainWindow *RegistrationModule)
       {


            if (RegistrationModule->objectName().isEmpty())
            {
            RegistrationModule->setObjectName(QString::fromUtf8("RegistrationModule"));
            }


            QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
            sizePolicy.setHorizontalStretch(0);
            sizePolicy.setVerticalStretch(0);
            sizePolicy.setHeightForWidth(RegistrationModule->sizePolicy().hasHeightForWidth());
            RegistrationModule->setSizePolicy(sizePolicy);


            centralWidget = new QWidget(RegistrationModule);

            regOut1 = new QVTKWidget();
            regOut1 -> setMinimumWidth(256);
            // regOut1 -> setMinimumHeight(256);

            regOut2 = new QVTKWidget();
            regOut2 -> setMinimumWidth(256);
            // regOut2 -> setMinimumHeight(256);

            regIn1 = new QVTKWidget();
            regIn1 -> setMinimumWidth(256);
            //regIn1 -> setMinimumHeight(256);

            regIn2 = new QVTKWidget();
            regIn2 -> setMinimumWidth(256);
            //regIn2 -> setMinimumHeight(256);

            CTIn = new QVTKWidget();
            CTIn -> setMinimumWidth(256);
            // CTIn -> setMinimumHeight(256);

            outputH = new QHBoxLayout();
            outputV1 = new QVBoxLayout();
            outputV2 = new QVBoxLayout();

            mainlayout = new QVBoxLayout();
            mainHBox   = new QHBoxLayout();
            LoadV2 = new QVBoxLayout();

            loadButtons = new QVBoxLayout();
            LoadV = new QVBoxLayout();

            V_outPut = new QVBoxLayout();
            V_outPut1 = new QVBoxLayout();
            corr_tmatrix = new QHBoxLayout();

            QVBoxLayout* Rot = new QVBoxLayout();
            QVBoxLayout* Trans = new QVBoxLayout();

            QVBoxLayout* res = new QVBoxLayout();
            QVBoxLayout* iso = new QVBoxLayout();

            RotTransF = new QHBoxLayout();
            Proj = new QVBoxLayout();
            projAngle1 = new QHBoxLayout() ;
            projAngle2 = new QHBoxLayout() ;

            res1L = new QVBoxLayout();
            res2L = new QVBoxLayout();
            BoxH  = new QHBoxLayout();

            center1 = new QVBoxLayout();
            center2 = new QVBoxLayout();
            BoxH3  = new QHBoxLayout();

            regAndresults = new QVBoxLayout();
            _2D3DRegRes = new QPushButton();
            thresholdWithL = new QVBoxLayout();
            scdWithL =  new QVBoxLayout();

            QHBoxLayout* iso1L = new QHBoxLayout();
            QHBoxLayout* iso2L = new QHBoxLayout();
            QHBoxLayout* iso3L = new QHBoxLayout();
            Resample = new QComboBox();
            Resample -> insertItem(1,"Full DRR");
            Resample -> insertItem(2,"Downsampled DRR");
            progressbar = new QProgressBar()  ;

            proj1 = new QDoubleSpinBox();
            proj1->setSingleStep(0.1);
            proj1->setDecimals(2);
            proj1->setRange(0.0, 360.0);

            proj2 = new QDoubleSpinBox();
            proj2->setSingleStep(0.1);
            proj2->setDecimals(2);
            proj2->setRange(0.0, 360.0);

            QLabel *projs = new QLabel("Projection angles");

            Proj -> addWidget(projs);
            Proj -> addWidget(proj1);
            Proj -> addWidget(proj2);

            BoxH -> addLayout(Proj);

            tx = new QDoubleSpinBox();
            tx->setSingleStep(0.1);
            tx->setDecimals(2);
            tx->setRange(-10.0, 10.0);

            ty = new QDoubleSpinBox();
            ty->setSingleStep(0.1);
            ty->setDecimals(2);
            ty->setRange(-10.0, 10.0);

            tz = new QDoubleSpinBox();
            tz->setSingleStep(0.1);
            tz->setDecimals(2);
            tz->setRange(-10.0, 10.0);

            cx1 = new QDoubleSpinBox();
            cx1->setSingleStep(0.1);
            cx1->setDecimals(2);
            cx1->setRange(-100.0, 100.0);

            cx2 = new QDoubleSpinBox();
            cx2->setSingleStep(0.1);
            cx2->setDecimals(2);
            cx2->setRange(-35.0, 35.0);

            cy1 = new QDoubleSpinBox();
            cy1->setSingleStep(0.1);
            cy1->setDecimals(2);
            cy1->setRange(-35.0, 35.0);

            cy2 = new QDoubleSpinBox();
            cy2->setSingleStep(0.1);
            cy2->setDecimals(2);
            cy2->setRange(-35.0, 35.0);



            QLabel *t_label = new QLabel("Translation");
            t_label ->setMargin(0.5);
            Trans -> addWidget(t_label);
            Trans -> addWidget(tx);
            Trans -> addWidget(ty);
            Trans -> addWidget(tz);


            rx = new QDoubleSpinBox();
            rx->setSingleStep(0.1);
            rx->setDecimals(2);
            rx->setRange(-90, 90);

            ry = new QDoubleSpinBox();
            ry->setSingleStep(0.1);
            ry->setDecimals(2);
            ry->setRange(-90, 90);
            rz = new QDoubleSpinBox();
            rz->setSingleStep(0.1);
            rz->setDecimals(2);
            rz->setRange(-90, 90);
            QLabel *r_label = new QLabel("Rotation");

            Rot -> addWidget(r_label);
            Rot -> addWidget(rx);
            Rot -> addWidget(ry);
            Rot -> addWidget(rz);

            inputImage2D1 = new QPushButton();
            //inputImage2D1 -> setMaximumWidth(100);
            inputImage2D2 = new QPushButton();
            //inputImage2D2 -> setMaximumWidth(100);
            inputCTVolume = new QPushButton();
            //inputCTVolume -> setMaximumWidth(100);

            loadButtons -> addWidget (inputImage2D1);
            loadButtons -> addWidget (inputImage2D2);
            loadButtons -> addWidget (inputCTVolume);

            RotTransF -> addLayout(loadButtons);
            RotTransF -> addLayout(Rot);
            RotTransF -> addLayout(Trans);

            res1 = new QDoubleSpinBox();
            res1->setSingleStep(0.1);
            res1->setDecimals(2);
            res1->setRange(0.0, 10.0);
            res1->setSuffix(" mm");
            res1 ->setValue(1.00);

            res2 = new QDoubleSpinBox();
            res2->setSingleStep(0.1);
            res2->setDecimals(2);
            res2->setRange(0.0, 10.0);
            res2->setSuffix(" mm");
            res2 ->setValue(1.00);

            res3 = new QDoubleSpinBox();
            res3->setSingleStep(0.1);
            res3->setDecimals(2);
            res3->setRange(0.0, 10.0);
            res3->setSuffix(" mm");
            res3 ->setValue(1.00);


            res4 = new QDoubleSpinBox();
            res4->setSingleStep(0.1);
            res4->setDecimals(2);
            res4->setRange(0.0, 10.0);
            res4->setSuffix(" mm");
            res4 ->setValue(1.00);

            checkRes1 = new QCheckBox("Resolution 1");
            checkRes1 -> setStyleSheet("QLabel { background-color : black; color : white; }");
            checkRes2 = new QCheckBox("Resolution 2");

            res1L -> addWidget(checkRes1);
            res1L -> addWidget(res1);
            res1L -> addWidget(res2);

            res2L -> addWidget(checkRes2);
            res2L -> addWidget(res3);
            res2L -> addWidget(res4);

            BoxH -> addLayout(res1L);
            BoxH -> addLayout(res2L);


            checkCenter1= new QCheckBox("Center 1");
            checkCenter2= new QCheckBox("Center 2");

            center1->addWidget(checkCenter1);
            center1->addWidget(cx1);
            center1->addWidget(cy1);

            center2->addWidget(checkCenter2);
            center2->addWidget(cx2);
            center2->addWidget(cy2);


            checkThres = new QCheckBox("Threshold");

            threshold = new QDoubleSpinBox();
            threshold->setSingleStep(0.1);
            threshold->setDecimals(2);
            threshold->setRange(-1000, 1000);

//            thresholdWithL -> addWidget(checkThres);
//            thresholdWithL -> addWidget(threshold);

            QHBoxLayout *HB_thresh = new QHBoxLayout();


            HB_thresh -> addWidget(checkThres,Qt::AlignCenter);
            HB_thresh -> addWidget(threshold,Qt::AlignCenter);


            checkSCD = new QCheckBox("SID");
            scd = new QDoubleSpinBox();
            scd->setSingleStep(0.1);
            scd->setDecimals(2);
            scd->setRange(0, 3000);
            scd ->setValue(1000.0);


//            scdWithL -> addWidget(checkSCD);
//            scdWithL -> addWidget(scd);


            QHBoxLayout *HB_SCD = new QHBoxLayout();

            HB_SCD -> addWidget(checkSCD,Qt::AlignCenter);
            HB_SCD -> addWidget(scd,Qt::AlignCenter);


            QHBoxLayout *sidandthre = new QHBoxLayout();
            sidandthre -> addStretch(1);
            sidandthre -> addLayout(HB_SCD,3);
            sidandthre -> addStretch(1);
            sidandthre -> addLayout(HB_thresh,3);
            sidandthre -> addStretch(1);


//            BoxVtc = new QVBoxLayout();
//            BoxVtc -> addLayout(scdWithL);
//            BoxVtc -> addLayout(thresholdWithL);

            BoxH ->addLayout(center1);
            BoxH ->addLayout(center2);


            iso1 = new QDoubleSpinBox();
            iso1->setDecimals(2);
            iso1->setRange(0.0, 500.0);

            iso2 = new QDoubleSpinBox();
            iso2->setSingleStep(0.1);
            iso2->setDecimals(2);
            iso2->setRange(0.0, 500.0);

            iso3 = new QDoubleSpinBox();
            iso3->setSingleStep(0.1);
            iso3->setDecimals(2);
            iso3->setRange(0.0, 500.0);


            checkIso = new QCheckBox("Isocenter");
            checkIsoL = new QHBoxLayout();


            QFrame* line6 = new QFrame();
            line6->setFrameShape(QFrame::HLine);
            line6->setFrameShadow(QFrame::Sunken);

            iso -> addWidget(checkIso);
            iso -> addWidget (iso1);
            iso -> addWidget (iso2);
            iso -> addWidget (iso3);

            RotTransF -> addLayout(iso);

            QFrame* line1 = new QFrame();
            line1->setFrameShape(QFrame::HLine);
            line1->setFrameShadow(QFrame::Sunken);

            QFrame* line2 = new QFrame();
            line2->setFrameShape(QFrame::HLine);
            line2->setFrameShadow(QFrame::Sunken);

            QFrame* line3 = new QFrame();
            line3->setFrameShape(QFrame::HLine);
            line3->setFrameShadow(QFrame::Sunken);

            QFrame* line4 = new QFrame();
            line4->setFrameShape(QFrame::HLine);
            line4->setFrameShadow(QFrame::Sunken);

            mainlayout->addLayout(RotTransF);
            mainlayout -> addWidget(line1);
            mainlayout -> addLayout(BoxH);
            mainlayout -> addLayout(sidandthre);
            mainlayout->addWidget(line2);



            _2D3DReg    = new QPushButton();
            saveDRR     = new QPushButton();
            generateDRR = new QPushButton();

//            regAndresults -> addWidget(_2D3DReg);
//            regAndresults -> addWidget(_2D3DRegRes);

            QHBoxLayout *resultandReg = new QHBoxLayout();
			resampleratio = new QDoubleSpinBox();
			resampleratio->setSingleStep(0.1);
            resampleratio->setDecimals(2);
            resampleratio->setRange(1.0, 10);
            resultandReg ->addStretch(1);
            resultandReg -> addWidget(resampleratio);
            resultandReg -> addWidget(_2D3DReg,4);
            resultandReg -> addWidget(_2D3DRegRes,4);
            resultandReg->addStretch(1);

//            performSave = new QVBoxLayout();
//            performSave -> addWidget(Resample);
//            performSave -> addWidget(generateDRR);
//            performSave -> addWidget(saveDRR);


            QHBoxLayout *performandSave = new QHBoxLayout();
            performandSave -> addWidget(Resample);
            performandSave -> addWidget(generateDRR);
            performandSave -> addWidget(saveDRR);

            HBox6 = new QHBoxLayout();

//            HBox6 -> addLayout(BoxVtc);
//            HBox6 -> addLayout(regAndresults);
//            HBox6 -> addLayout(performSave);

            QFrame* line5 = new QFrame();
            line5->setFrameShape(QFrame::HLine);
            line5->setFrameShadow(QFrame::Sunken);

//            mainlayout -> addLayout(HBox6);
            mainlayout -> addLayout(performandSave);
            mainlayout -> addLayout(performandSave);
            mainlayout->addLayout(resultandReg);


            QWidget *controlContainer = new QWidget();
            controlContainer->setLayout(mainlayout);

            QFrame* line8 = new QFrame();
            line8->setFrameShape(QFrame::HLine);
            line8->setFrameShadow(QFrame::Sunken);

            QLabel *Input1 = new QLabel("Reference Image 1");
            Input1 -> setStyleSheet("font-size:16px; ");


            QLabel *Input2 = new QLabel("Reference Image 2");
            Input2 -> setStyleSheet("font-size:16px; ");

            LoadV -> addWidget(Input1);
            LoadV -> addWidget(regIn1);

            LoadV -> addWidget(Input2);
            LoadV -> addWidget(regIn2);

            controlsRestrictorWidget3 = new QWidget();
            controlsRestrictorWidget3 -> setLayout(LoadV);


            QLabel *DRR1 = new QLabel("DRR 1");

            DRR1 -> setStyleSheet("font-size:16px; ");

            QLabel *DRR2 = new QLabel("DRR 2");

            DRR2 -> setStyleSheet("font-size:16px; ");

            outputV1 ->addWidget(DRR1);
            outputV1 ->addWidget(regOut1);
            outputV2 ->addWidget(DRR2);
            outputV2 ->addWidget(regOut2);




            QLabel *CTLabel = new QLabel("CT Volume");
            CTLabel -> setStyleSheet("font-size:16px; ");

            V_outPut -> addWidget(controlContainer);
            V_outPut -> addWidget (line6);
            V_outPut -> addWidget(CTLabel);
            V_outPut -> addWidget(CTIn);
            V_outPut -> addWidget(line8);
            V_outPut -> addWidget(progressbar);

            controlsRestrictorWidget1 = new QWidget();
            controlsRestrictorWidget1 -> setLayout(V_outPut);
            controlsRestrictorWidget1 -> setMinimumWidth(256);

            LoggerDisplay = new QTextEdit();

            //LoadV2 -> addWidget(LoggerDisplay);
            LoadV2 -> addLayout(outputV1);
            LoadV2 -> addLayout(outputV2);
            controlsRestrictorWidget2 = new QWidget();
            controlsRestrictorWidget2 -> setLayout(LoadV2);

            mainHBox->addWidget(controlsRestrictorWidget3,4);
            mainHBox->addWidget(controlsRestrictorWidget1,6);
            mainHBox->addWidget (controlsRestrictorWidget2,4);


            centralWidget->setLayout(mainHBox);
            RegistrationModule->setCentralWidget(centralWidget);
            retranslateUi(RegistrationModule);

            QMetaObject::connectSlotsByName (RegistrationModule);
  }
 void retranslateUi(QMainWindow *RegistrationModule)
    {
        RegistrationModule -> setWindowTitle(QApplication::translate("RegistrationModule", " ", 0, QApplication::UnicodeUTF8));
        inputImage2D1 -> setText(QApplication::translate("RegistrationModule", "Load image 1", 0, QApplication::UnicodeUTF8));
        inputImage2D2 -> setText(QApplication::translate("RegistrationModule", "Load image 2", 0, QApplication::UnicodeUTF8));
        inputCTVolume-> setText(QApplication::translate("RegistrationModule", "Load CT Volume", 0, QApplication::UnicodeUTF8));
        _2D3DReg -> setText(QApplication::translate("RegistrationModule", "Apply Registeration", 0, QApplication::UnicodeUTF8));
        saveDRR -> setText(QApplication::translate("RegistrationModule", "Save DRR", 0, QApplication::UnicodeUTF8));
        generateDRR -> setText(QApplication::translate("RegistrationModule", "Render DRR", 0,QApplication::UnicodeUTF8));
        _2D3DRegRes -> setText(QApplication::translate("RegistrationModule", "Registeration Results", 0, QApplication::UnicodeUTF8));

    }

};

namespace Ui {
      class RegistrationModule : public Ui_RegistrationModule {};
  }
QT_END_NAMESPACE// namespace Ui
#endif // UI_REGISTRATIONMODULE_H
