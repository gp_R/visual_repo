#ifndef TOLOGGER_H
#define TOLOGGER_H

#include "itkProcessObject.h"
#include "itkImage.h"
#include "itkTwoImageToOneImageMetric.h"
#include "itkSingleValuedNonLinearOptimizer.h"
#include "itkDataObjectDecorator.h"
#include "itkTwoProjectionImageRegistrationMethod.h"
#include "itkPowellOptimizer.h"

#include <QtCore>
#include <QThread>





class toLogger : public QObject
{
Q_OBJECT
public:
		toLogger();

signals:
    void fromOptimizer(double);
		 
private:
		typedef itk::TwoProjectionImageRegistrationMethod
	          	<itk::Image< float, 3 > ,itk::Image< float, 3 > >    RegistrationType  ;
						
       //typedef RegistrationType::Pointer ParametersType;
public:
		unsigned int observeCurrentIter(unsigned int itr);
		double observeNCCValue(double value);
		//ParametersType observeParam(ParametersType param);
};
#endif
