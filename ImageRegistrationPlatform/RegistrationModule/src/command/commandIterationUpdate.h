#include "itkCommandIterationUpdate.h"
#include "itkCommand.h"
#include <vtkSmartPointer.h>
#include "itkPowellOptimizer.h"
#include "toLogger.h"

class CommandIterationUpdate : public itk::Command
							 
    {
    public:
    	typedef  CommandIterationUpdate    Self;
    	typedef  itk::Command              Superclass;
    	typedef  itk::SmartPointer<Self>   Pointer;
   	itkNewMacro( Self );

    protected:
    	CommandIterationUpdate() {};


    public:
    	typedef itk::PowellOptimizer      OptimizerType;
    	typedef const OptimizerType       *OptimizerPointer;
		OptimizerPointer optimizer;
	void Execute(itk::Object *caller, const itk::EventObject & event)
    {
        this->Execute( (const itk::Object *)caller, event);
    }

    void Execute(const itk::Object *object, const itk::EventObject & event)
    {
        optimizer =
            dynamic_cast< OptimizerPointer >( object );
        toLogger *logger = new toLogger();
        if( typeid( event ) != typeid( itk::IterationEvent ) )
        {
            return;
        }
        std::cout << "Iteration: " << optimizer->GetCurrentIteration() << std::endl;
        std::cout << "Similarity: " << optimizer-> GetValue() << std::endl;
        std::cout << "Position: " << optimizer-> GetCurrentPosition() << std::endl;
        int v = logger ->observeCurrentIter(optimizer->GetCurrentIteration());
//        LOG_DEBUG("From Obtimizer");
        qDebug()<< "Called from optimizer: Thread id = " << QThread::currentThreadId();
        //std::cout << "Iteration: " << v << std::endl;

    }

    	
};

