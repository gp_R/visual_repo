#ifndef LOG_LEVEL_HH
#define LOG_LEVEL_HH

typedef enum LOG_LEVEL
{
    INFO,           // Informative
    DEBUG,          // Debugging
    WARNING,        // Warning
    ERRORd,          // Error
    DEBUG_GL_LOOP,  // Logged within an OpenGL rendering loop 'performance'
    EXIT
}LOG_LEVEL;

#endif // LOG_LEVEL_HH

