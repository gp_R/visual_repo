#include "tiffDisplay.h"

tiffDisplay :: tiffDisplay(vtkRenderWindow* win) : window_(win)
{
	    LOG_DEBUG("Initializing The Tiff Renderer");
	    actor_1= vtkSmartPointer<vtkImageActor>::New();
	    actor_2= vtkSmartPointer<vtkImageActor>::New();
        camera_ = vtkSmartPointer<vtkCamera>::New();
        camera_-> SetViewUp (0, -1, 0);
		noWindow_ = window_ == NULL ? true:false;
		renderer_ = vtkSmartPointer<vtkRenderer>::New();
		interactor_ = vtkSmartPointer<vtkRenderWindowInteractor>::New();
		style_	  = vtkSmartPointer<vtkInteractorStyleImage>::New();
		
		stack = vtkSmartPointer<vtkImageStack>::New();
		interactor_ ->SetInteractorStyle(style_);

		if(noWindow_)
		{	
			LOG_DEBUG("Initializing window");
			window_ = vtkRenderWindow::New();
			interactor_ ->SetRenderWindow(window_);
			LOG_DEBUG("done window");
		}
        interactor_ = window_->GetInteractor();
		interactor_ -> SetInteractorStyle(style_);
		MatTransform  = vtkSmartPointer<vtkMatrix4x4>::New();
		
		//opacity = 0.5;
        isDataLoaded = false;
}

void tiffDisplay:: loadData(char *fileName)
{
	LOG_DEBUG("loading Data from %s", fileName);
    reader_= vtkSmartPointer<vtkTIFFReader>::New();
    reader_->SetFileName(fileName);
    reader_->Update();
	actor_1 -> SetInputData(reader_ -> GetOutput ());
	renderer_ -> AddActor(actor_1);
    isDataLoaded = true;
}

void tiffDisplay:: RenderTiff(){
	
		if(!isDataLoaded)
			LOG_ERRORd("You must Load Data First");
		LOG_DEBUG("Update Tiff Renderer");
		updateRenderer();
}

void tiffDisplay::RenderTiffData(itk::RescaleIntensityImageFilter< IImageType, OImageType > ::Pointer rescaleFilter){

		LOG_DEBUG("Connector Initialized");
		ConnectorType::Pointer rescaledConnector = ConnectorType::New();
		rescaledConnector->SetInput(rescaleFilter->GetOutput());
		rescaledConnector->Update();
		actor_2-> SetInputData(rescaledConnector->GetOutput());
		renderer_ -> AddActor(actor_2);
		
		updateRenderer();
}

	
void tiffDisplay::RenderTiffWithRefence(itk::RescaleIntensityImageFilter< IImageType, OImageType > ::Pointer rescaleFilter,vtkSmartPointer<vtkTIFFReader> reader_){

		LOG_DEBUG("Connector Initialized");
		ConnectorType::Pointer rescaledConnector = ConnectorType::New();
		rescaledConnector->SetInput(rescaleFilter->GetOutput());
		rescaledConnector->Update();
		
		LOG_DEBUG("Get Image Data 1");
		vtkImageData* imageData1 = rescaledConnector -> GetOutput();
		double * origin1 = imageData1 -> GetOrigin();
		imageData1->SetOrigin(0,0,0);
		actor_2-> SetInputData(imageData1);

		LOG_DEBUG("Get Image Data 2");
		vtkImageData* imageData2 = reader_ -> GetOutput();
		double * origin2= imageData2 -> GetOrigin();
		imageData2->SetOrigin(0,0,0);
		actor_1->  SetInputData(imageData2 );
        
		//actor_1 -> RotateZ(20);
		actor_2 -> SetOpacity(opacity);
		//actor_2 -> SetUserMatrix(MatTransform);
		stack->AddImage(actor_1);
		
		stack->AddImage(actor_2);
		
		LOG_DEBUG("Stack set");
		renderer_->AddViewProp(stack);
		updateRenderer();
}

void tiffDisplay::setTransform(double rotX, double rotY, double rotZ, double transX,double transY,double transZ){
	// First map from ITK coord systen to QVTK coord system 
	vtkrotX = rotX; 
	vtkrotZ = rotY; 
	vtkrotY = rotZ;
	vtktransX = transX;
	vtktransZ = transY; 
	vtktransY = transZ;
	
	double transformationMat [16] = { 
								
							 cos(vtkrotY) * cos(vtkrotZ) ,
							-cos(vtkrotY) * sin(vtkrotZ), 
							 sin(vtkrotY), 
							 vtktransX,  /* element 3 for transX */
							 sin(vtkrotX) * sin(vtkrotY) * cos(vtkrotZ) + cos(vtkrotX) * sin(vtkrotZ),
							-sin(vtkrotX) * sin(vtkrotY) * sin(vtkrotZ) + cos(vtkrotX) * cos(vtkrotZ),
							-sin(vtkrotX) * cos(vtkrotY),
							 vtktransY, /* element 3 for transX */
							-cos(vtkrotX) * sin(vtkrotY) * cos(vtkrotZ) + sin(vtkrotX) * sin(vtkrotZ),
							 cos(vtkrotX) * sin(vtkrotY) * sin(vtkrotZ) + sin(vtkrotX) * cos(vtkrotZ),
							 cos(vtkrotX) * cos(vtkrotY),
							 vtktransZ ,  /* element 3 for transX */
							 0,0,0,1
							 };
	
	
	MatTransform ->  DeepCopy(transformationMat);

	
}
void tiffDisplay::updateRenderer()
{
    renderer_->SetActiveCamera(camera_);
    renderer_->ResetCamera();
    renderer_->SetBackground(0, 0, 0);
    window_->AddRenderer(renderer_);
    window_->Render();
    //interactor_->SetRenderWindow(renderer_);
	//interactor_ ->Start();    
    

}


vtkSmartPointer<vtkTIFFReader> tiffDisplay::getReader(){
	return reader_;
}

vtkSmartPointer<vtkImageActor> tiffDisplay::getActor(){
	return actor_1;
}


void tiffDisplay::updateOpacity(double opacity){
	LOG_DEBUG("Opacity Update value %f", opacity);
    actor_2 -> SetOpacity(opacity);
    actor_2 -> Update();
    actor_1 -> Update();
    stack -> Update();
    //stack -> Update();
    updateRenderer();
    
}
