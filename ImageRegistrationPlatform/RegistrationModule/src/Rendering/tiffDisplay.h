#ifndef TIFFDISPLAY_H
#define TIFFDISPLAY_H

#include <vtkTIFFReader.h>
#include <vtkImageViewer2.h>
#include <vtkRenderWindow.h>
#include <vtkImageActor.h>
#include <vtkCamera.h>
#include "QVTKWidget.h"
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkPolyDataMapper.h>
#include "vtkImageMapper3D.h"
#include <vtkRenderWindowInteractor.h>
#include "itkRescaleIntensityImageFilter.h"
#include "itkImageToVTKImageFilter.h"
#include "itkVTKImageToImageFilter.h"

#include <vtkInteractorStyleImage.h>
#include "Logger.h"

#include <vtkImageResliceMapper.h>
#include <vtkImageStack.h>

#include <vtkImageBlend.h>
#include <vtkImageAppend.h>
#include <vtkImageMapToColors.h>
#include <vtkLookupTable.h>
#include <vtkMatrix4x4.h>
class tiffDisplay{
	
	public : 
		/**
		* @brief tiffDisplay
		*/
		explicit tiffDisplay(vtkRenderWindow* win = NULL);

		/**
		* @brief loadData
		* @param dirName
		*/
		void loadData(char* dirName);


		void RenderTiff();
		
		
		void RenderTiffData(itk::RescaleIntensityImageFilter<
			itk::Image< float, 3 >, itk::Image< unsigned char, 3 > > ::Pointer rescaleFilter);
		
		
		void RenderTiffWithRefence(itk::RescaleIntensityImageFilter<
			itk::Image< float, 3 >, itk::Image< unsigned char, 3 > > ::Pointer rescaleFilter,vtkSmartPointer<vtkTIFFReader> reader_);
		 
		vtkSmartPointer<vtkTIFFReader>   getReader();
		vtkSmartPointer<vtkImageActor>   getActor();
		
		void setTransform(double rotX, double rotY, double rotZ, double transX,double transY,double transZ);
        void updateOpacity(double opacity);

	private :
	
	/**
     * @brief fileName
     */
	
	char *fileName;
	/**
     * @brief reader_
     */
    vtkSmartPointer<vtkTIFFReader>  reader_;

  
	/**
     * @brief actor_1, actor_2
     */
	vtkSmartPointer<vtkImageActor> actor_1;
	vtkSmartPointer<vtkImageActor> actor_2;

    /**
     * @brief window_
     */
    vtkRenderWindow* window_;
	
    /**
     * @brief renderer_
     */
    vtkSmartPointer<vtkRenderer> renderer_;
    
	/**
     * @brief camera_
     */
    vtkSmartPointer<vtkCamera> camera_;


	/**
     * @brief stack
     */
	vtkSmartPointer<vtkImageStack> stack;


	/**
     * @brief interactor_
     */
    vtkSmartPointer<vtkRenderWindowInteractor> interactor_;
	
	vtkSmartPointer<vtkInteractorStyleImage> style_ ; 
    
	/**
     * @brief itk-vtk connector
     */
    typedef itk::Image<unsigned char, 3>  OImageType;
	typedef itk::Image<float, 3>  IImageType;
    typedef itk::ImageToVTKImageFilter<OImageType> ConnectorType;
    typedef itk::VTKImageToImageFilter<OImageType> ConnectorTypeV;

     
	/**
     * @brief itk-vtk transfrom map 
     */
    double vtkrotZ;
    double vtkrotY;
    double vtkrotX;
    
    double vtktransZ;
    double vtktransY; 
	double vtktransX; 
	
    /**
     * @brief opacity
     */

    float opacity;

	/**
     * @brief MatTransform 
     */
	 vtkSmartPointer<vtkMatrix4x4>  MatTransform;
    public:

    /**
     * @brief isDataLoaded
     */
    bool isDataLoaded;

    bool noWindow_;


    private:

    /**
    * @brief updateRenderer
    */
    void updateRenderer();
	


};

#endif

