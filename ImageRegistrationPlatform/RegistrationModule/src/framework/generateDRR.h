#ifndef GENERATEDRR_H
#define GENERATEDRR_H


// The transformation used is a rigid 3D Euler transform with the
// provision of a center of rotation which defaults to the center of
// the 3D volume. In practice the center of the particular feature of
// interest in the 3D volume should be used.

#include "itkEuler3DTransform.h"


// This is an intensity based registration algorithm so ray casting is
// used to project the 3D volume onto pixels in the target 2D image.
#include "itkSiddonJacobsRayCastInterpolateImageFunction.h"

#include "itkImage.h"
#include "itkGDCMImageIO.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkImageSeriesReader.h"

#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkResampleImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkFlipImageFilter.h"


#include "itkTimeProbesCollectorBase.h"

#include "tiffDisplay.h"
#include <vtkMarchingCubes.h>
#include <vtkActor.h>
#include <vtkStripper.h>
#include <vtkPolyDataMapper.h>

#include "Logger.h"
#include<QObject>

#pragma GCC visibility push(hidden)
class generateDRR : public QObject {
	
	public:
		generateDRR();
		//~generateDRR();

	public slots:
		void renderDRR(double projAngle1, double projAngle2, double rx, double ry, double rz,
							double tx, double ty, double tz, double cx, double cy, double cz, double scd, double  threshold, 
							double image1resX, double image1resY, double image2resX, double image2resY,
							double image1centerX, double image1centerY, double image2centerX, double image2centerY,
							QString volumeDir_, bool iso, bool center1, bool center2, bool spacing1,
							bool spacing2,bool Threshold, bool SCD,bool sizeState);

    
	private:
		static const    unsigned int    Dimension = 3;
		typedef  float           InternalPixelType;
		typedef  short           PixelType3D;
	
		typedef itk::Image< PixelType3D, Dimension > ImageType3D;
	
		typedef unsigned char	OutputPixelType;
		typedef itk::Image< OutputPixelType, Dimension > OutputImageType;
	
		typedef itk::Image< InternalPixelType, Dimension > InternalImageType;
	
		typedef itk::RescaleIntensityImageFilter<
					InternalImageType, OutputImageType > RescaleFilterType;
					
		RescaleFilterType::Pointer rescaler_1;
		RescaleFilterType::Pointer rescaler_2;
		
		typedef itk::Euler3DTransform< double >  TransformType;

		
		typedef itk::SiddonJacobsRayCastInterpolateImageFunction<
		        InternalImageType,double > InterpolatorType;


			
		typedef itk::ImageFileReader< InternalImageType > ImageReaderType2D;
	
		typedef itk::ImageSeriesReader < ImageType3D >  ReaderType;
		ReaderType::Pointer reader;
	
		typedef itk::GDCMImageIO       ImageIOType;
	    ImageIOType::Pointer dicomIO;

		typedef itk::GDCMSeriesFileNames NamesGeneratorType;
	    NamesGeneratorType::Pointer nameGenerator;
	    
	    // Container 
	    typedef std::vector< std::string >    SeriesIdContainer;
	    typedef std::vector< std::string >   FileNamesContainer;
		typedef itk::FlipImageFilter< InternalImageType > FlipFilterType;
		typedef FlipFilterType::FlipAxesArrayType FlipAxesArrayType;
	
		typedef itk::RescaleIntensityImageFilter<
			InternalImageType, InternalImageType > Input2DRescaleFilterType;
	
		typedef itk::CastImageFilter<
			ImageType3D, InternalImageType > CastFilterType3D;
			
		// constant for converting degrees to radians
		typedef ImageType3D::RegionType      ImageRegionType3D;
		typedef ImageRegionType3D::SizeType  SizeType3D;
		typedef itk::ResampleImageFilter< InternalImageType, InternalImageType > ResampleFilterType;
			
		double threshold;
        double scd;
        double image1centerX;
        double image1centerY;
        double image2centerX;
        double image2centerY;
			
		int dx;
		int dy;
        double outputspacing;
	public:
		itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3> >
							   ::Pointer getrescaler1();
							   
		itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer  getrescaler2();
							   
		void setOutSpacingSize(double outputspacing);

			


	signals:
		//void started();
		//void finished();
	
		
	};
#pragma GCC visibility pop
#endif
