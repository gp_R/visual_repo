
#ifndef TWOPROJ2D3DREG_H
#define TWOPROJ2D3DREG_H
#include "itkTwoProjectionImageRegistrationMethod.h"


// The transformation used is a rigid 3D Euler transform with the
// provision of a center of rotation which defaults to the center of
// the 3D volume. In practice the center of the particular feature of
// interest in the 3D volume should be used.

#include "itkEuler3DTransform.h"

// We have chosen to implement the registration using the normalized coorelation
// similarity measure.

#include "itkNormalizedCorrelationTwoImageToOneImageMetric.h"

// This is an intensity based registration algorithm so ray casting is
// used to project the 3D volume onto pixels in the target 2D image.
#include "itkSiddonJacobsRayCastInterpolateImageFunction.h"

// Finally the Powell optimizer is used to avoid requiring gradient information.
#include "itkPowellOptimizer.h"
#include "itkImage.h"
#include "itkGDCMImageIO.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkImageSeriesReader.h"

#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkResampleImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkFlipImageFilter.h"


#include "itkTimeProbesCollectorBase.h"

#include "tiffDisplay.h"
#include <vtkMarchingCubes.h>
#include <vtkActor.h>
#include <vtkStripper.h>
#include <vtkPolyDataMapper.h>

#include "Logger.h"
#include<QObject>
#include <QtCore>
#include <QThread>
#include <QString>

#pragma GCC visibility push(hidden)
class twoProj2D3DReg : public QObject {
	Q_OBJECT
	public:
		twoProj2D3DReg();
		//~twoProj2D3DReg();


        void setRegistrationParameters(double projAngle1, double projAngle2, double rx, double ry, double rz,
                            double tx, double ty, double tz, double cx, double cy, double cz, double scd, double threshold ,
                            double image1resX, double image1resY, double image2resX, double image2resY,
                            double image1centerX, double image1centerY, double image2centerX, double image2centerY,
                            QString volumeDir_,QString fileInput_1,QString fileInput_2,
                            bool iso, bool center1, bool center2, bool spacing1, bool spacing2, bool Threshold, bool SCD,bool sizeState);
        itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer getInverseRescaler1();
		itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer getInverseRescaler2();

	public slots:

		void setPerformingRegistrationON();

        void testThreading();  // fot test only

		void getSignalfromtoLoggerandEmitUpdateSignaltoMainThred(double);
    
	private:

    /*-----------------Registration Parameteres and Add-ons for threading ||| to be optimized later ------------------------ */ 
	double R_projAngle1, R_projAngle2,  R_rx,  R_ry,  R_rz, R_tx,  R_ty,  R_tz,  R_cx,  R_cy,  R_cz,  R_scd, R_threshold, R_image1resX, 
		    R_image1resY,  R_image2resX,  R_image2resY, R_image1centerX,  R_image1centerY,  R_image2centerX,  R_image2centerY;
	QString R_volumeDir_, R_fileInput_1, R_fileInput_2;
	bool R_iso,  R_center1,  R_center2,  R_spacing1,  R_spacing2, R_Threshold,  R_SCD, R_sizeState;


	void performRegistration(double projAngle1, double projAngle2, double rx, double ry, double rz,
						double tx, double ty, double tz, double cx, double cy, double cz, double scd, double threshold ,
						double image1resX, double image1resY, double image2resX, double image2resY,
						double image1centerX, double image1centerY, double image2centerX, double image2centerY,
						QString volumeDir_,QString fileInput_1,QString fileInput_2,
						bool iso, bool center1, bool center2, bool spacing1, bool spacing2, bool Threshold, bool SCD,bool sizeState);



		static const    unsigned int    Dimension = 3;
		typedef  float           InternalPixelType;
		typedef  short           PixelType3D;
	
		typedef itk::Image< PixelType3D, Dimension > ImageType3D;
	
		typedef unsigned char	OutputPixelType;
		typedef itk::Image< OutputPixelType, Dimension > OutputImageType;
	
		typedef itk::Image< InternalPixelType, Dimension > InternalImageType;
	
		typedef itk::RescaleIntensityImageFilter<
					InternalImageType, OutputImageType > RescaleFilterType;
		RescaleFilterType::Pointer rescaler_1;
		RescaleFilterType::Pointer rescaler_2;
		RescaleFilterType::Pointer invrescaler_1;
		RescaleFilterType::Pointer invrescaler_2;

		typedef itk::Euler3DTransform< double >  TransformType;

		typedef itk::PowellOptimizer   OptimizerType;

		typedef itk::NormalizedCorrelationTwoImageToOneImageMetric<
				InternalImageType,InternalImageType >    MetricType;

		typedef itk::SiddonJacobsRayCastInterpolateImageFunction<
		        InternalImageType,double > InterpolatorType;


		typedef itk::TwoProjectionImageRegistrationMethod<
			InternalImageType,
			InternalImageType >   RegistrationType;
			
		typedef itk::ImageFileReader< InternalImageType > ImageReaderType2D;
	
		typedef itk::ImageSeriesReader < ImageType3D >  ReaderType;
		ReaderType::Pointer reader;
	
		typedef itk::GDCMImageIO       ImageIOType;
	    ImageIOType::Pointer dicomIO;

		typedef itk::GDCMSeriesFileNames NamesGeneratorType;
	    NamesGeneratorType::Pointer nameGenerator;
	    
	    // Container 
	    typedef std::vector< std::string >    SeriesIdContainer;
	    typedef std::vector< std::string >   FileNamesContainer;
		typedef itk::FlipImageFilter< InternalImageType > FlipFilterType;
		typedef FlipFilterType::FlipAxesArrayType FlipAxesArrayType;
	
		typedef itk::RescaleIntensityImageFilter<
			InternalImageType, InternalImageType > Input2DRescaleFilterType;
	
		typedef itk::CastImageFilter<
			ImageType3D, InternalImageType > CastFilterType3D;
			
		// constant for converting degrees to radians
		typedef ImageType3D::RegionType      ImageRegionType3D;
		typedef ImageRegionType3D::SizeType  SizeType3D;
		typedef itk::ResampleImageFilter< InternalImageType, InternalImageType > ResampleFilterType;

		double threshold;
        double scd;
        double image1centerX;
        double image1centerY;
        double image2centerX;
        double image2centerY;

        double RotationAlongX;
		double RotationAlongY;
		double RotationAlongZ;
		double TranslationAlongX;
		double TranslationAlongY;
		double TranslationAlongZ;

		int numberOfIterations;

		double bestValue ;
	    double outputspacing;

        
	public:
		itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3> >
							   ::Pointer getrescaler1();
							   
		itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer  getrescaler2();
							   
		 
		 double getRotationAlongX();
		 double getRotationAlongY();
		 double getRotationAlongZ();
		
		 double getTranslationAlongX();
		 double getTranslationAlongY();
		 double getTranslationAlongZ();
		 double getNumberOfIterations();
		 double getBestValue();
		 void setOutSpacingSize(double outputspacing);
	signals:
		void started();
        void FINISHED();
        void updateLogger(double);

	};
#pragma GCC visibility pop
#endif
