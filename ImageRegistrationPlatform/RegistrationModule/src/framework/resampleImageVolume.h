/* This Program read a 3D image volume, downsample it.*/

#ifndef RESAMPLEIMAGEVOLUME_H
#define RESAMPLEIMAGEVOLUME_H

#include "itkImage.h"
#include "itkResampleImageFilter.h"
#include "itkAffineTransform.h"

#include<QObject>

#pragma GCC visibility push(hidden)
class resampleImageVolume : public QObject {
	
	public:
		resampleImageVolume(double outputspacing);
		//~resampleImageVolume();

    
	private:
		static const    unsigned int    Dimension = 3;
		double outputSpacing[ Dimension ];
		typedef   short  InputPixelType;
		typedef   short  OutputPixelType;

		typedef itk::Image< InputPixelType,  Dimension >   InputImageType;
		typedef itk::Image< OutputPixelType, Dimension >   OutputImageType;
		typedef itk::ResampleImageFilter<
					InputImageType, OutputImageType >  FilterType;
					
		FilterType::Pointer filter;
		typedef itk::AffineTransform< double, Dimension >  TransformType;
		TransformType::Pointer transform;
		typedef itk::LinearInterpolateImageFunction<
					InputImageType, double >  InterpolatorType;
		InterpolatorType::Pointer interpolator ;
        double resampleRatio[ Dimension ];
   
		
	public slots:
	itk::Image< short,  3 > ::Pointer  resampleVolume(itk::Image< short,  3 > ::Pointer InputImage);
		
	};
#pragma GCC visibility pop
#endif
