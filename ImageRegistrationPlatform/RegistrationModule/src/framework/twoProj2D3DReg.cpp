#include "twoProj2D3DReg.h"
#include "commandIterationUpdate.h"
#include "resampleImageVolume.h"
#include "toLogger.h"

twoProj2D3DReg::twoProj2D3DReg(){

}


// the registration method itself.
	//	emit updateText("Initialization .. ");	

void twoProj2D3DReg::getSignalfromtoLoggerandEmitUpdateSignaltoMainThred(double value)
{
	qDebug() << "Signal from toLogger is recieved, trying to emit it to main thread: Threadd id " << QThread::currentThreadId();
	// emit updateLogger(value);
}	
void twoProj2D3DReg::testThreading(){
   int i = 1;
    while(i!=0){
        std::cout << " Hello, Threading" << i << std::endl;
        i++;

    }
    emit FINISHED();


}

/**
	To get the registrations parameters from the main thread (GuI) and set them to be passed to 
	{performRegistration} later via setPerformingRegistrationON
*/

void twoProj2D3DReg :: setRegistrationParameters(double projAngle1, double projAngle2, double rx, double ry, double rz,
							double tx, double ty, double tz, double cx, double cy, double cz, double scd, double  threshold, 
							double image1resX, double image1resY, double image2resX, double image2resY,
							double image1centerX, double image1centerY, double image2centerX, double image2centerY,
							QString volumeDir_,QString fileInput_1,QString fileInput_2,
                            bool iso, bool center1, bool center2, bool spacing1, bool spacing2,bool Threshold, bool SCD,bool sizeState){

			R_projAngle1 = projAngle1;
			R_projAngle2 = projAngle2;

			R_rx =  rx;   R_ry = ry,   R_rz = rz; 
			R_tx =  tx;   R_ty = ty;   R_tz = tz; 
			R_cx =  cx;   R_cy = cy;   R_cz = cz;

			R_scd = scd;


            R_threshold  = threshold;
			R_image1resX = image1resX; 
			R_image1resY = image1resY;
			R_image2resX = image2resX; 
			R_image2resY = image2resY;

			R_image1centerX =   image1centerX; 
			R_image1centerY =   image1centerY;
			R_image2centerX =   image2centerX;   
			R_image2centerY =   image2centerY;

			R_volumeDir_ =  volumeDir_;
			R_fileInput_1=  fileInput_1;
			R_fileInput_2 = fileInput_2;

			R_iso       =  iso;
			R_center1   =  center1;
			R_center2   =  center2;
			R_spacing1  =  spacing1;
			R_spacing2  =  spacing2;
            R_Threshold =  Threshold;
			R_SCD = SCD;
			R_sizeState = sizeState;	
			LOG_DEBUG("Parameters are set");

}


/**
   We need a slot with no parameters to be prosscced by the the registration thread, so {setPerformingRegistrationON} is
   called when the thread start signal emitted to call and perform the registration process.
   I made it this ugly way to not change {performRegistration} structure, you can handle this now.
*/

void twoProj2D3DReg::setPerformingRegistrationON(){

    LOG_DEBUG("Thread starting point:: performing registration");
	qDebug() << "Call from registrtion thread :"  << QThread::currentThreadId();


   


	this->performRegistration( R_projAngle1,  R_projAngle2,  R_rx,  R_ry,  R_rz,
							 R_tx,  R_ty,  R_tz,  R_cx,  R_cy,  R_cz,  R_scd,   R_threshold, 
							 R_image1resX,  R_image1resY,  R_image2resX,  R_image2resY,
							 R_image1centerX,  R_image1centerY,  R_image2centerX,  R_image2centerY,
							 R_volumeDir_, R_fileInput_1, R_fileInput_2,
                             R_iso,  R_center1,  R_center2,  R_spacing1,  R_spacing2, R_Threshold,  R_SCD, R_sizeState);
    
	LOG_DEBUG("Thread End point:: Registration is Done!");
    emit FINISHED();  //Main thread listens to this signal
}


void twoProj2D3DReg :: performRegistration(double projAngle1, double projAngle2, double rx, double ry, double rz,
							double tx, double ty, double tz, double cx, double cy, double cz, double scd, double  threshold, 
							double image1resX, double image1resY, double image2resX, double image2resY,
							double image1centerX, double image1centerY, double image2centerX, double image2centerY,
							QString volumeDir_,QString fileInput_1,QString fileInput_2,
                            bool iso, bool center1, bool center2, bool spacing1, bool spacing2,bool Threshold, bool SCD,bool sizeState){


	LOG_DEBUG("Thread point:: Registration process is started!");

 	// toLogger *logger = new toLogger();
	// connect(logger,SIGNAL(fromOptimizer(double)), this, SLOT(getSignalfromtoLoggerandEmitUpdateSignaltoMainThred(double)));
	
	if(Threshold)
		this -> threshold = threshold;
	
	

	if(SCD)
		this -> scd = scd;
	
	

	std::cout << " scd = " << scd  << " mm" << std::endl;
	std::cout << " threshold = " << threshold  << " " << std::endl;

	// Each of the registration components are instantiated in the
	// usual way...

	// emit started();
	MetricType::Pointer         metric        = MetricType::New();
	TransformType::Pointer      transform     = TransformType::New();
	OptimizerType::Pointer      optimizer     = OptimizerType::New();
	InterpolatorType::Pointer   interpolator1  = InterpolatorType::New();
	InterpolatorType::Pointer   interpolator2  = InterpolatorType::New();
	RegistrationType::Pointer   registration  = RegistrationType::New();

    LOG_DEBUG("Components are instantiated");

	metric->ComputeGradientOff();
	metric->SetSubtractMean( true );

	// and passed to the registration method:

	registration->SetMetric(        metric        );
	registration->SetOptimizer(     optimizer     );
	registration->SetTransform(     transform     );
	registration->SetInterpolator1(  interpolator1  );
	registration->SetInterpolator2(  interpolator2  );



	// for * .img files
	//typedef itk::ImageFileReader< ImageType3D > ImageReaderType3D;
	//ImageReaderType3D::Pointer imageReader3D = ImageReaderType3D::New();

	// for DICOM Series

    LOG_DEBUG("Reader Type Initialized");
	dicomIO = ImageIOType::New();
	reader = ReaderType::New();
	
	reader->SetImageIO( dicomIO );

	LOG_DEBUG("DICOM Type Set");

	nameGenerator = NamesGeneratorType::New();
    nameGenerator->SetDirectory((char*) volumeDir_.toStdString().c_str());

	const SeriesIdContainer & seriesUID = nameGenerator->GetSeriesUIDs();
    SeriesIdContainer::const_iterator seriesItr = seriesUID.begin();
    SeriesIdContainer::const_iterator seriesEnd = seriesUID.end();
    
    while( seriesItr != seriesEnd )
      {
      std::cout << seriesItr->c_str() << std::endl;
      ++seriesItr;
      }
     LOG_DEBUG("Now Reading DICOM Series");

	std::string seriesIdentifier;
	seriesIdentifier = seriesUID.begin()->c_str();
	
	
    FileNamesContainer fileNames;
    fileNames = nameGenerator->GetFileNames( seriesIdentifier );

    
	ImageReaderType2D::Pointer imageReader2D1 = ImageReaderType2D::New();
	ImageReaderType2D::Pointer imageReader2D2 = ImageReaderType2D::New();

	imageReader2D1 -> SetFileName  ( (char*) fileInput_1.toStdString().c_str() );
	imageReader2D2 -> SetFileName  ( (char*) fileInput_2.toStdString().c_str() );

	// for * .img files
	//imageReader3D  -> SetFileName  ( (char*) volumeDir_.toStdString().c_str() );
	//imageReader3D  -> Update();
	//ImageType3D::Pointer image3DIn = imageReader3D->GetOutput();


	// for DICOM 
	reader->SetFileNames( fileNames );
	reader -> Update();
	ImageType3D::Pointer image3DIn = reader->GetOutput();
    LOG_DEBUG("DICOM LOADED");
    //emit updateText("DICOM LOADED.. ");	

	if(sizeState){
		resampleImageVolume *resmplevol = new resampleImageVolume(outputspacing);
		image3DIn = resmplevol -> resampleVolume(image3DIn);
	}
	// To simply Siddon-Jacob's fast ray-tracing algorithm, we force the origin of the CT image
	// to be (0,0,0). Because we align the CT isocenter with the central axis, the projection
	// geometry is fully defined. The origin of the CT image becomes irrelavent.
	ImageType3D::PointType image3DOrigin;
	image3DOrigin[0] = 0.0;
	image3DOrigin[1] = 0.0;
	image3DOrigin[2] = 0.0;
	image3DIn->SetOrigin(image3DOrigin);

	InternalImageType::Pointer image_tmp1 = imageReader2D1->GetOutput();
	InternalImageType::Pointer image_tmp2 = imageReader2D2->GetOutput();

	imageReader2D1->Update();
	imageReader2D2->Update();

	InternalImageType::SpacingType spacing;
	if (spacing1)
	{
		 LOG_DEBUG("Resolution1 checked");
		spacing[0] = image1resX;
		spacing[1] = image1resY;
		spacing[2] = 1.0;  // Z spacing
		image_tmp1->SetSpacing( spacing );
	}

	if (spacing2)
	{
		LOG_DEBUG("Resolution2 checked");
		spacing[0] = image2resX;
		spacing[1] = image2resY;
	    spacing[2] = 1.0;  // Z spacing
		image_tmp2->SetSpacing( spacing );
	}

	
	// The input 2D images were loaded as 3D images. They were considered
	// as a single slice from a 3D volume. By default, images stored on the
	// disk are treated as if they have RAI orientation. After view point
	// transformation, the order of 2D image pixel reading is equivalent to
	// from inferior to superior. This is contradictory to the traditional
	// 2D x-ray image storage, in which a typical 2D image reader reads and
	// writes images from superior to inferior. Thus the loaded 2D DICOM
	// images should be flipped in y-direction. This was done by using a.
	// FilpImageFilter.
	
	FlipFilterType::Pointer flipFilter1 = FlipFilterType::New();
	FlipFilterType::Pointer flipFilter2 = FlipFilterType::New();

	
	FlipAxesArrayType flipArray;
	flipArray[0] = 0;
	flipArray[1] = 1;
	flipArray[2] = 0;

	flipFilter1->SetFlipAxes( flipArray );
	flipFilter2->SetFlipAxes( flipArray );
	flipFilter1->SetInput( imageReader2D1->GetOutput() );
	flipFilter2->SetInput( imageReader2D2->GetOutput() );

	// The input 2D images may have 16 bits. We rescale the pixel value to between 0-255.
	

	Input2DRescaleFilterType::Pointer rescaler2D1 = Input2DRescaleFilterType::New();
	rescaler2D1->SetOutputMinimum(   0 );
	rescaler2D1->SetOutputMaximum( 255 );
	rescaler2D1->SetInput( flipFilter1->GetOutput() );

	Input2DRescaleFilterType::Pointer rescaler2D2 = Input2DRescaleFilterType::New();
	rescaler2D2->SetOutputMinimum(   0 );
	rescaler2D2->SetOutputMaximum( 255 );
	rescaler2D2->SetInput( flipFilter2->GetOutput() );


	//  The 3D CT dataset is casted to the internal image type using
	//  {CastImageFilters}.

	

	CastFilterType3D::Pointer caster3D = CastFilterType3D::New();
	caster3D->SetInput( image3DIn );

	rescaler2D1->Update();
	rescaler2D2->Update();
	caster3D->Update();


	registration->SetFixedImage1(  rescaler2D1->GetOutput() );
	registration->SetFixedImage2(  rescaler2D2->GetOutput() );
	registration->SetMovingImage( caster3D->GetOutput() );

	// Initialise the transform
	// ~~~~~~~~~~~~~~~~~~~~~~~~

	// Set the order of the computation. Default ZXY
	transform->SetComputeZYX(true);


	// The transform is initialised with the translation [tx,ty,tz] and
	// rotation [rx,ry,rz] specified on the command line

	TransformType::OutputVectorType translation;
	
	translation[0] = tx;
	translation[1] = ty;
	translation[2] = tz;

	transform->SetTranslation(translation);

	// constant for converting degrees to radians
	const double dtr = ( atan(1.0) * 4.0 ) / 180.0;
	transform->SetRotation(dtr*rx, dtr*ry, dtr*rz);
	
	// The centre of rotation is set by default to the centre of the 3D
	// volume but can be offset from this position using a command
	// line specified translation [cx,cy,cz]

	ImageType3D::PointType origin3D = image3DIn->GetOrigin();
	const itk::Vector<double, 3> resolution3D = image3DIn->GetSpacing();

	

	ImageRegionType3D region3D = caster3D->GetOutput()->GetBufferedRegion();
	SizeType3D        size3D   = region3D.GetSize();
	TransformType::InputPointType isocenter;
	LOG_DEBUG("SizeX :%d",size3D[0]);
	LOG_DEBUG("Sizey :%d",size3D[1]);
	LOG_DEBUG("Sizez :%d",size3D[2]);
	
  if (iso)
  {
  // Isocenter location given by the user.
    LOG_DEBUG("Iso checked");
    isocenter[0] = origin3D[0] + resolution3D[0] * cx;
    isocenter[1] = origin3D[1] + resolution3D[1] * cy;
    isocenter[2] = origin3D[2] + resolution3D[2] * cz;
    
  }
  else
  {
	      LOG_DEBUG("Iso is not checked");

    // Set the center of the image as the isocenter.
    isocenter[0] = origin3D[0] + resolution3D[0] * static_cast<double>( size3D[0] ) / 2.0;
    isocenter[1] = origin3D[1] + resolution3D[1] * static_cast<double>( size3D[1] ) / 2.0;
    isocenter[2] = origin3D[2] + resolution3D[2] * static_cast<double>( size3D[2] ) / 2.0;
  }

  transform->SetCenter(isocenter);


  // Set the origin of the 2D image
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// For correct (perspective) projection of the 3D volume, the 2D
	// image needs to be placed at a certain distance (the source-to-
	// isocenter distance {scd} ) from the focal point, and the normal
	// from the imaging plane to the focal point needs to be specified.
	//
	// By default, the imaging plane normal is set by default to the
	// center of the 2D image but may be modified from this using the
	// command line parameters [image1centerX, image1centerY,
	// image2centerX, image2centerY].

	double origin2D1[ Dimension ];
	double origin2D2[ Dimension ];

	// Note: Two 2D images may have different image sizes and pixel dimensions, although
	// scd are the same.

	const itk::Vector<double, 3> resolution2D1 = imageReader2D1->GetOutput()->GetSpacing();
	const itk::Vector<double, 3> resolution2D2 = imageReader2D2->GetOutput()->GetSpacing();

	typedef InternalImageType::RegionType      ImageRegionType2D;
	typedef ImageRegionType2D::SizeType  SizeType2D;

	ImageRegionType2D region2D1 = rescaler2D1->GetOutput()->GetBufferedRegion();
	ImageRegionType2D region2D2 = rescaler2D2->GetOutput()->GetBufferedRegion();
	SizeType2D        size2D1   = region2D1.GetSize();
	SizeType2D        size2D2   = region2D2.GetSize();



  // Central axis positions are given by the user. Use the image centers
	// as the central axis position.

	if(!center1){
		LOG_DEBUG("center 1 checked");
		image1centerX = ((double) size2D1[0] - 1.)/2.;
		image1centerY = ((double) size2D1[1] - 1.)/2.;		
	}

	// 2D Image 1
	origin2D1[0] = - resolution2D1[0] * image1centerX;
	origin2D1[1] = - resolution2D1[1] * image1centerY;
	origin2D1[2] = - scd;
	imageReader2D1->GetOutput()->SetOrigin( origin2D1 );
	rescaler2D1->GetOutput()->SetOrigin( origin2D1 );

	if(!center2){
		LOG_DEBUG("center 2 checked");
		image2centerX = ((double) size2D2[0] - 1.)/2.;
		image2centerY = ((double) size2D2[1] - 1.)/2.;
	}

	
	// 2D Image 2
	origin2D2[0] = - resolution2D2[0] * image2centerX;
	origin2D2[1] = - resolution2D2[1] * image2centerY;
	origin2D2[2] = - scd;
	imageReader2D2->GetOutput()->SetOrigin( origin2D2 );
	rescaler2D2->GetOutput()->SetOrigin( origin2D2 );

	registration->SetFixedImageRegion1( rescaler2D1->GetOutput()->GetBufferedRegion() );
	registration->SetFixedImageRegion2( rescaler2D2->GetOutput()->GetBufferedRegion() );

  // Initialize the ray cast interpolator
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// The ray cast interpolator is used to project the 3D volume. It
	// does this by casting rays from the (transformed) focal point to
	// each (transformed) pixel coordinate in the 2D image.
	//
	// In addition a threshold may be specified to ensure that only
	// intensities greater than a given value contribute to the
	// projected volume. This can be used, for instance, to remove soft
	// tissue from projections of CT data and force the registration
	// to find a match which aligns bony structures in the images.

	// 2D Image 1
	interpolator1->SetProjectionAngle( dtr*projAngle1 );
	interpolator1->SetFocalPointToIsocenterDistance(scd);
	interpolator1->SetThreshold(threshold);
	interpolator1->SetTransform(transform);

	interpolator1->Initialize();

	// 2D Image 2
	interpolator2->SetProjectionAngle( dtr*projAngle2 );
	interpolator2->SetFocalPointToIsocenterDistance(scd);
	interpolator2->SetThreshold(threshold);
	interpolator2->SetTransform(transform);

	interpolator2->Initialize();
  // Set up the transform and start position
  	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  	// The registration start position is intialised using the
  	// transformation parameters.

  	registration->SetInitialTransformParameters( transform->GetParameters() );

  	// We wish to minimize the negative normalized correlation similarity measure.

    //optimizer->SetMaximize( true );  // for GradientDifferenceTwoImageToOneImageMetric
  	optimizer->SetMaximize( false );  // for NCC

  	optimizer->SetMaximumIteration( 10 );
  	optimizer->SetMaximumLineIteration( 4 ); // for Powell's method
  	optimizer->SetStepLength( 4.0 );
  	optimizer->SetStepTolerance( 0.02 );
  	optimizer->SetValueTolerance( 0.001 );

  	// The optimizer weightings are set such that one degree equates to
  	// one millimeter.

  	itk::Optimizer::ScalesType weightings( transform->GetNumberOfParameters() );
  	
  	weightings[0] = 1./dtr;
  	weightings[1] = 1./dtr;
  	weightings[2] = 1./dtr;
  	weightings[3] = 1.;
  	weightings[4] = 1.;
  	weightings[5] = 1.;

  	optimizer->SetScales( weightings );

    // Create the observers
	  // ~~~~~~~~~~~~~~~~~~~~

	  CommandIterationUpdate::Pointer observer = CommandIterationUpdate::New();
	  optimizer->AddObserver( itk::IterationEvent(), observer );
	  
	   // Start the registration
	   // ~~~~~~~~~~~~~~~~~~~~~~
     // Create a timer to record calculation time.
	  itk::TimeProbesCollectorBase timer;
////////////////////////////////////////////////////     Start Registration    /////////////////////////////////////////////////
    try
    {
      timer.Start("Registration");
      // Start the registration.
      registration->StartRegistration();
      timer.Stop("Registration");
    }
    catch( itk::ExceptionObject & err )
    {
      std::cout << "ExceptionObject caught !" << std::endl;
      std::cout << err << std::endl;
      // return -1;
    }
////////////////////////////////////////////////////     End Registration    /////////////////////////////////////////////////

    //diplay correlation factor

    typedef RegistrationType::ParametersType ParametersType;
    ParametersType finalParameters = registration->GetLastTransformParameters();
	
	// Convert radian to degree
    RotationAlongX = finalParameters[0]/dtr; 
    RotationAlongY = finalParameters[1]/dtr;
    RotationAlongZ = finalParameters[2]/dtr;
    TranslationAlongX = finalParameters[3];
    TranslationAlongY = finalParameters[4];
    TranslationAlongZ = finalParameters[5];
	


	
    numberOfIterations = optimizer->GetCurrentIteration();

    bestValue = optimizer->GetValue();

	  std::cout << "Result = " << std::endl;
	  std::cout << " Rotation Along X = " << RotationAlongX  << " deg" << std::endl;
	  std::cout << " Rotation Along Y = " << RotationAlongY  << " deg" << std::endl;
	  std::cout << " Rotation Along Z = " << RotationAlongZ  << " deg" << std::endl;
	  std::cout << " Translation X = " << TranslationAlongX  << " mm" << std::endl;
	  std::cout << " Translation Y = " << TranslationAlongY  << " mm" << std::endl;
	  std::cout << " Translation Z = " << TranslationAlongZ  << " mm" << std::endl;
	  std::cout << " Number Of Iterations = " << numberOfIterations << std::endl;
	  std::cout << " Metric value  = " << bestValue<< std::endl;
	  


    //  ui-> text-> append("4");
	// ui -> text -> setText("Correlation Value = " + QString::number(bestValue));
    // Write out the projection images at the registration position
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	TransformType::Pointer finalTransform = TransformType::New();
	// The transform is determined by the parameters and the rotation center.
	finalTransform->SetParameters( finalParameters );
	finalTransform->SetCenter(isocenter);

	// The ResampleImageFilter is the driving force for the projection image generation.
	ResampleFilterType::Pointer resampleFilter1 = ResampleFilterType::New();

	resampleFilter1->SetInput( caster3D->GetOutput() ); // Link the 3D volume.
	resampleFilter1->SetDefaultPixelValue( 0 );

	// The parameters of interpolator1, such as ProjectionAngle and scd
	// have been set before registration. Here we only need to replace the initial
	// transform with the final transform.
	interpolator1->SetTransform( finalTransform );
	interpolator1->Initialize();
	resampleFilter1->SetInterpolator( interpolator1 );

	// The output 2D projection image has the same image size, origin, and the pixel spacing as
	// those of the input 2D image.
	resampleFilter1->SetSize( rescaler2D1->GetOutput()->GetLargestPossibleRegion().GetSize() );
	resampleFilter1->SetOutputOrigin(  rescaler2D1->GetOutput()->GetOrigin() );
	resampleFilter1->SetOutputSpacing( rescaler2D1->GetOutput()->GetSpacing() );

	// Do the same thing for the output image 2.
	ResampleFilterType::Pointer resampleFilter2 = ResampleFilterType::New();
	resampleFilter2->SetInput( caster3D->GetOutput() );
	resampleFilter2->SetDefaultPixelValue( 0 );

	// The parameters of interpolator2, such as ProjectionAngle and scd
	// have been set before registration. Here we only need to replace the initial
	// transform with the final transform.
	interpolator2->SetTransform( finalTransform );
	interpolator2->Initialize();
	resampleFilter2->SetInterpolator( interpolator2 );

	resampleFilter2->SetSize( rescaler2D2->GetOutput()->GetLargestPossibleRegion().GetSize() );
	resampleFilter2->SetOutputOrigin(  rescaler2D2->GetOutput()->GetOrigin() );
	resampleFilter2->SetOutputSpacing( rescaler2D2->GetOutput()->GetSpacing() );

  // As explained before, the computed projection is upsided-down.
	// Here we use a FilpImageFilter to flip the images in y-direction.
	flipFilter1->SetInput( resampleFilter1->GetOutput() );
	flipFilter2->SetInput( resampleFilter2->GetOutput() );

	rescaler_1 = RescaleFilterType::New();
	rescaler_1->SetOutputMinimum(   0 );
	rescaler_1->SetOutputMaximum( 255 );
	rescaler_1->SetInput( flipFilter1->GetOutput() );

	rescaler_2 = RescaleFilterType::New();
	rescaler_2->SetOutputMinimum(   0 );
	rescaler_2->SetOutputMaximum( 255 );
	rescaler_2->SetInput( flipFilter2->GetOutput() );

	rescaler_1->Update();
	rescaler_2->Update();


	
	timer.Report();
	


}

itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer twoProj2D3DReg :: getInverseRescaler1()
{

		return invrescaler_1;


}
itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer twoProj2D3DReg :: getInverseRescaler2()
{

	 	 return invrescaler_2;

}


itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer twoProj2D3DReg :: getrescaler1()
{

		return rescaler_1;


}
itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer twoProj2D3DReg :: getrescaler2()
{

	 	 return rescaler_2;

}

 double twoProj2D3DReg::getRotationAlongX(){
	return RotationAlongX;

}
	
 double twoProj2D3DReg::getRotationAlongY(){
	return RotationAlongY;

}
 double twoProj2D3DReg::getRotationAlongZ(){
	return RotationAlongZ;
	
}		
 double twoProj2D3DReg::getTranslationAlongX(){
	return TranslationAlongX;

}
 double twoProj2D3DReg::getTranslationAlongY(){
	return TranslationAlongY;

}
 double twoProj2D3DReg::getTranslationAlongZ(){
	return TranslationAlongZ;

}
 double twoProj2D3DReg::getNumberOfIterations(){
	return numberOfIterations;

}
 double twoProj2D3DReg::getBestValue(){
	return bestValue;

}


void twoProj2D3DReg::setOutSpacingSize(double outputspacing){

			this -> outputspacing = outputspacing;
}

