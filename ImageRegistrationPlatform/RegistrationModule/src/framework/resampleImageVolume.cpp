#include "resampleImageVolume.h"
#include "Logger.h"


resampleImageVolume::resampleImageVolume(double outputspacing){
  
  
  outputSpacing[0] = outputspacing; // pixel spacing in millimeters along X
  outputSpacing[1] = outputspacing; // pixel spacing in millimeters along Y
  outputSpacing[2] = outputspacing; // pixel spacing in millimeters along Z
  filter = FilterType::New();
  transform = TransformType::New();
  interpolator = InterpolatorType::New();
  
  filter->SetInterpolator( interpolator );
  filter->SetDefaultPixelValue( 0 );

}


itk::Image< short,  3 > ::Pointer  resampleImageVolume::
							resampleVolume(itk::Image< short,  3 > ::Pointer InputImage){

  InputImageType::SpacingType inputSpacing = InputImage ->GetSpacing();
  InputImageType::RegionType inputRegion   = InputImage ->GetLargestPossibleRegion();
  InputImageType::SizeType inputSize 	   = inputRegion.GetSize();
  std::cout<<inputSize[0];
  std::cout<<inputSize[1];
  std::cout<<inputSize[2];
  resampleRatio[0] = outputSpacing[0] / inputSpacing[0]; // resample ratio along X
  resampleRatio[1] = outputSpacing[1] / inputSpacing[1]; // resample ratio along Y
  resampleRatio[2] = outputSpacing[2] / inputSpacing[2]; // resample ratio along Z
  LOG_DEBUG("resamplingRx %f" , resampleRatio[0]); 
  LOG_DEBUG("resamplingRy %f" , resampleRatio[1]); 
  LOG_DEBUG("resamplingRz %f" , resampleRatio[2]); 

  filter->SetOutputSpacing( outputSpacing );

  filter->SetOutputOrigin( InputImage->GetOrigin() );


  InputImageType::SizeType   outputSize;
  outputSize[0] = floor(inputSize[0] / resampleRatio[0] + 0.5);  // number of pixels along X
  outputSize[1] = floor(inputSize[1] / resampleRatio[1] + 0.5);  // number of pixels along Y
  outputSize[2] = floor(inputSize[2] / resampleRatio[2] + 0.5);  // number of pixels along Z
  std::cout<<outputSize[0];
  std::cout<<outputSize[1];
  std::cout<<outputSize[2];

  filter->SetSize( outputSize );

  filter->SetInput( InputImage );

  transform->SetIdentity();
  filter->SetTransform( transform );
  filter -> Update();
  return filter-> GetOutput();
}
