#include "generateDRR.h"
#include "resampleImageVolume.h"

generateDRR::generateDRR(){
	
	
	}


// the registration method itself.
	//	emit updateText("Initialization .. ");	

	
void generateDRR :: renderDRR(double projAngle1, double projAngle2, double rx, double ry, double rz,
							double tx, double ty, double tz, double cx, double cy, double cz, double scd, double  threshold, 
							double image1resX, double image1resY, double image2resX, double image2resY,
							double image1centerX, double image1centerY, double image2centerX, double image2centerY,
							QString volumeDir_, bool iso, bool center1, bool center2, bool spacing1, bool spacing2,
							bool Threshold, bool SCD,bool sizeState){

	//emit started();
	qDebug("Started ");
	LOG_DEBUG("STARTED");
	// Size of the output image in number of pixels
	
	dx = 256;
	dy = 256;

		
	if(Threshold)
		this -> threshold = threshold;
	
	

	if(SCD)
		this -> scd = scd;
	
	TransformType::Pointer      transform     = TransformType::New();
	InterpolatorType::Pointer   interpolator1  = InterpolatorType::New();
	InterpolatorType::Pointer   interpolator2  = InterpolatorType::New();

	

	// for * .img files
	//typedef itk::ImageFileReader< ImageType3D > ImageReaderType3D;
	//ImageReaderType3D::Pointer imageReader3D = ImageReaderType3D::New();

	// for DICOM Series

    LOG_DEBUG("Reader Type Initialized");

	
	  
	dicomIO = ImageIOType::New();
	reader = ReaderType::New();
	
	reader->SetImageIO( dicomIO );

	LOG_DEBUG("DICOM Type Set");

	nameGenerator = NamesGeneratorType::New();
    nameGenerator->SetDirectory((char*) volumeDir_.toStdString().c_str());

	const SeriesIdContainer & seriesUID = nameGenerator->GetSeriesUIDs();
    SeriesIdContainer::const_iterator seriesItr = seriesUID.begin();
    SeriesIdContainer::const_iterator seriesEnd = seriesUID.end();
    
    while( seriesItr != seriesEnd )
      {
      std::cout << seriesItr->c_str() << std::endl;
      ++seriesItr;
      }
     LOG_DEBUG("Now Reading DICOM Series");
     //emit updateText("Now Reading DICOM Series .. ");	

	std::string seriesIdentifier;
	seriesIdentifier = seriesUID.begin()->c_str();
	
	
    FileNamesContainer fileNames;
    fileNames = nameGenerator->GetFileNames( seriesIdentifier );


	// for * .img files
	//imageReader3D  -> SetFileName  ( (char*) volumeDir_.toStdString().c_str() );
	//imageReader3D  -> Update();
	//ImageType3D::Pointer image3DIn = imageReader3D->GetOutput();


	// for DICOM 
	reader->SetFileNames( fileNames );
	reader -> Update();
	ImageType3D::Pointer image3DIn = reader->GetOutput();
	
	if(sizeState){
		resampleImageVolume *resmplevol = new resampleImageVolume(outputspacing);
		image3DIn = resmplevol -> resampleVolume(image3DIn);
	
	}
    LOG_DEBUG("DICOM LOADED");
    //emit updateText("DICOM LOADED.. ");	


	// To simply Siddon-Jacob's fast ray-tracing algorithm, we force the origin of the CT image
	// to be (0,0,0). Because we align the CT isocenter with the central axis, the projection
	// geometry is fully defined. The origin of the CT image becomes irrelavent.
	ImageType3D::PointType image3DOrigin;
	image3DOrigin[0] = 0.0;
	image3DOrigin[1] = 0.0;
	image3DOrigin[2] = 0.0;
	image3DIn->SetOrigin(image3DOrigin);


	InternalImageType::SpacingType spacing_1;
	if (spacing1)
	{
		 LOG_DEBUG("Resolution1 checked");
		spacing_1[0] = image1resX;
		spacing_1[1] = image1resY;
		spacing_1[2] = 1.0;  // Z spacing
		
	}

	else{
		spacing_1[0] = 1.0;  // pixel spacing along X of the 2D DRR image [mm]
		spacing_1[1] = 1.0;  // pixel spacing along Y of the 2D DRR image [mm]
		spacing_1[2] = 1.0; 	// slice thickness of the 2D DRR image [mm]


	}
	InternalImageType::SpacingType spacing_2;
	if (spacing2)
	{
		LOG_DEBUG("Resolution2 checked");
		spacing_2[0] = image2resX;
		spacing_2[1] = image2resY;
	    spacing_2[2] = 1.0;  // Z spacing
		
	}
	else{
		spacing_2[0] = 1.0;  // pixel spacing along X of the 2D DRR image [mm]
		spacing_2[1] = 1.0;  // pixel spacing along Y of the 2D DRR image [mm]
		spacing_2[2] = 1.0; 	// slice thickness of the 2D DRR image [mm]


	}

	
	
	

	// The input 2D images may have 16 bits. We rescale the pixel value to between 0-255.
	


	//  The 3D CT dataset is casted to the internal image type using
	//  {CastImageFilters}.

	

	CastFilterType3D::Pointer caster3D = CastFilterType3D::New();
	caster3D->SetInput( image3DIn );

	caster3D->Update();

    LOG_DEBUG("caster3D LOADED");

	// Initialise the transform
	// ~~~~~~~~~~~~~~~~~~~~~~~~

	// Set the order of the computation. Default ZXY
	transform->SetComputeZYX(true);


	// The transform is initialised with the translation [tx,ty,tz] and
	// rotation [rx,ry,rz] specified on the command line

	TransformType::OutputVectorType translation;

	translation[0] = tx;
	translation[1] = ty;
	translation[2] = tz;

	transform->SetTranslation(translation);

	// constant for converting degrees to radians
	const double dtr = ( atan(1.0) * 4.0 ) / 180.0;
	transform->SetRotation(dtr*rx, dtr*ry, dtr*rz);

	// The centre of rotation is set by default to the centre of the 3D
	// volume but can be offset from this position using a command
	// line specified translation [cx,cy,cz]

	ImageType3D::PointType origin3D = image3DIn->GetOrigin();
	const itk::Vector<double, 3> resolution3D = image3DIn->GetSpacing();

	

	ImageRegionType3D region3D = caster3D->GetOutput()->GetBufferedRegion();
	SizeType3D        size3D   = region3D.GetSize();
	TransformType::InputPointType isocenter;
	LOG_DEBUG("SizeX :%d",size3D[0]);
	LOG_DEBUG("Sizey :%d",size3D[1]);
	LOG_DEBUG("Sizez :%d",size3D[2]);
  if (iso)
  {
  // Isocenter location given by the user.
    LOG_DEBUG("Iso checked");
    isocenter[0] = origin3D[0] + resolution3D[0] * cx;
    isocenter[1] = origin3D[1] + resolution3D[1] * cy;
    isocenter[2] = origin3D[2] + resolution3D[2] * cz;
    
  }
  else
  {
	      LOG_DEBUG("Iso is not checked");

    // Set the center of the image as the isocenter.
    isocenter[0] = origin3D[0] + resolution3D[0] * static_cast<double>( size3D[0] ) / 2.0;
    isocenter[1] = origin3D[1] + resolution3D[1] * static_cast<double>( size3D[1] ) / 2.0;
    isocenter[2] = origin3D[2] + resolution3D[2] * static_cast<double>( size3D[2] ) / 2.0;
  }
	
  transform->SetCenter(isocenter);


	// Set the origin of the 2D image
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// The size and resolution of the output DRR image is specified via the filter.
	// setup the scene
	InternalImageType::SizeType   size;
	double origin2D1[ Dimension ];
	double origin2D2[ Dimension ];
	
	size[0] = dx; // number of pixels along X of the 2D DRR image
	size[1] = dy; // number of pixels along Y of the 2D DRR image
	size[2] = 1;  // only one slice
	
	 
	
	
	  
	// pixel spacing along Y of the 2D DRR image [mm]
	// slice thickness of the 2D DRR image [mm]
    // Central axis positions are given by the user. Use the image centers
	// as the central axis position.

	if(!center1){
		LOG_DEBUG("center 1 not checked");
		image1centerX = ((double) size[0] - 1.)/2.;
		image1centerY = ((double) size[1] - 1.)/2.;		
	}

	// 2D Image 1
	origin2D1[0] = - image1resX * image1centerX;
	origin2D1[1] = - image1resY * image1centerY;
	origin2D1[2] = - scd;
	
	if(!center2){
		LOG_DEBUG("center 2 not checked");
		image2centerX = ((double) size[0] - 1.)/2.;
		image2centerY = ((double) size[1] - 1.)/2.;
	}

	
	// 2D Image 2
	origin2D2[0] = - image2resX * image2centerX;
	origin2D2[1] = - image2resY * image2centerY;
	origin2D2[2] = -scd ;
	


	// Initialize the ray cast interpolator
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// The ray cast interpolator is used to project the 3D volume. It
	// does this by casting rays from the (transformed) focal point to
	// each (transformed) pixel coordinate in the 2D image.
	//
	// In addition a threshold may be specified to ensure that only
	// intensities greater than a given value contribute to the
	// projected volume. This can be used, for instance, to remove soft
	// tissue from projections of CT data and force the registration
	// to find a match which aligns bony structures in the images.

	// 2D Image 1
	interpolator1->SetProjectionAngle( dtr*projAngle1 );
	interpolator1->SetFocalPointToIsocenterDistance(scd);
	interpolator1->SetThreshold(threshold);
	interpolator1->SetTransform(transform);

	interpolator1->Initialize();
    LOG_DEBUG("interpolator1 initialized");

	// 2D Image 2
	interpolator2->SetProjectionAngle( dtr*projAngle2 );
	interpolator2->SetFocalPointToIsocenterDistance(scd);
	interpolator2->SetThreshold(threshold);
	interpolator2->SetTransform(transform);

	interpolator2->Initialize();
    LOG_DEBUG("interpolator2 initialized");

	// The ResampleImageFilter is the driving force for the projection image generation.
	ResampleFilterType::Pointer resampleFilter1 = ResampleFilterType::New();
	resampleFilter1->SetInput( caster3D->GetOutput() ); // Link the 3D volume.
	resampleFilter1->SetDefaultPixelValue( 0 );

	resampleFilter1->SetInterpolator( interpolator1 );

	// The output 2D projection image has the same image size, origin, and the pixel spacing as
	// those of the input 2D image.
	resampleFilter1->SetSize( size );
	resampleFilter1->SetOutputOrigin( origin2D1 );
		LOG_DEBUG("resampleFilter1 updated");

	resampleFilter1->SetOutputSpacing(spacing_1 );
	itk::TimeProbesCollectorBase timer;
	timer.Start("DRR generation");
	resampleFilter1 -> Update();
	timer.Stop("DRR generation");

	// Do the same thing for the output image 2.
	ResampleFilterType::Pointer resampleFilter2 = ResampleFilterType::New();
	resampleFilter2->SetInput( caster3D->GetOutput() );
	resampleFilter2->SetDefaultPixelValue( 0 );

	resampleFilter2->SetInterpolator( interpolator2 );

	resampleFilter2->SetSize( size );
	resampleFilter2->SetOutputOrigin(origin2D2);
		LOG_DEBUG("resampleFilter2	 updated");

	resampleFilter2->SetOutputSpacing( spacing_2 );

	resampleFilter2 -> Update();


	// The input 2D images were loaded as 3D images. They were considered
	// as a single slice from a 3D volume. By default, images stored on the
	// disk are treated as if they have RAI orientation. After view point
	// transformation, the order of 2D image pixel reading is equivalent to
	// from inferior to superior. This is contradictory to the traditional
	// 2D x-ray image storage, in which a typical 2D image reader reads and
	// writes images from superior to inferior. Thus the loaded 2D DICOM
	// images should be flipped in y-direction. This was done by using a.
	// FlippImageFilter.
	
	FlipFilterType::Pointer flipFilter1 = FlipFilterType::New();
	FlipFilterType::Pointer flipFilter2 = FlipFilterType::New();

	
	FlipAxesArrayType flipArray;
	flipArray[0] = 0;
	flipArray[1] = 1;
	flipArray[2] = 0;

	flipFilter1->SetFlipAxes( flipArray );
	flipFilter2->SetFlipAxes( flipArray );
	
	flipFilter1->SetInput( resampleFilter1->GetOutput() );
	flipFilter2->SetInput( resampleFilter2->GetOutput() );


	
	rescaler_1 = RescaleFilterType::New();
	rescaler_1->SetOutputMinimum(   0 );
	rescaler_1->SetOutputMaximum( 255);
	rescaler_1->SetInput( flipFilter1->GetOutput() );

	rescaler_2 = RescaleFilterType::New();
	rescaler_2->SetOutputMinimum(   0 );
	rescaler_2->SetOutputMaximum( 255 );
	rescaler_2->SetInput( flipFilter2->GetOutput() );

	rescaler_1->Update();
	rescaler_2->Update();
	timer.Report();

}
itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer generateDRR :: getrescaler1()
{

		return rescaler_1;


}
itk::RescaleIntensityImageFilter<itk::Image< float, 3 >, itk::Image< unsigned char, 3 > >
							   ::Pointer generateDRR :: getrescaler2()
{

	 	 return rescaler_2;

}

void generateDRR::setOutSpacingSize(double outputspacing){

			this -> outputspacing = outputspacing;
}

