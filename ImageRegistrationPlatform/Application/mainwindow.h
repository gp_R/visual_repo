#ifndef mainwindow_H
#define mainwindow_H

#include <sstream>
#include <QMainWindow>


namespace Ui {

class mainwindow;

}


class mainwindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit mainwindow(QWidget *parent = 0);
    ~mainwindow();


private:
    Ui::mainwindow *ui;


};

#endif // Vmainwindow_H
