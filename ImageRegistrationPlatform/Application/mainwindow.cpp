#include <sstream>

#include "mainwindow.h"
#include "ui_mainwindow.h"


using namespace std;

mainwindow::mainwindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mainwindow)
 {
    ui->setupUi(this);
    connect(this->ui->RegistrationWindow,SIGNAL(SHAREDIR(QString)),this->ui->VisualizationWindow,SLOT(setDirPath(QString)));
//    connect(this->ui->RegistrationWindow,SIGNAL(SHARERENDERER(vtkRenderer *)),
//            this->ui->VisualizationWindow,SLOT(setRenderer(vtkRenderer *)));

}

mainwindow::~mainwindow(){
  delete ui;
}
