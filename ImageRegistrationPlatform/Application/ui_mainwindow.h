
#ifndef UI_mainwindowH
#define UI_mainwindow_H

#include <QtGui/QApplication>

#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QHBoxLayout>
#include <QtGui/QWidget>
#include <QTabWidget>
#include <QStatusBar>

#include "VisualizationModule.h"
#include "RegistrationModule.h"

QT_BEGIN_NAMESPACE

class Ui_mainwindow
{
public:
    QWidget *centralWidget;
    QPushButton *button;
    QVBoxLayout *VB_widowLayout;

    VisualizationModule *VisualizationWindow;
    RegistrationModule  *RegistrationWindow;

    void setupUi(QMainWindow *mainwindow)
    {
        if (mainwindow->objectName().isEmpty())
        {
            mainwindow->setObjectName(QString::fromUtf8("mainwindow"));
        }


         mainwindow->setMinimumSize(1200,900);
         QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
         sizePolicy.setHorizontalStretch(2);
         sizePolicy.setVerticalStretch(2);
         sizePolicy.setHeightForWidth(mainwindow->sizePolicy().hasHeightForWidth());
         mainwindow->setSizePolicy(sizePolicy);
   
        centralWidget = new QWidget(mainwindow);        

        QWidget *widget =   new QWidget();
        button  = new QPushButton("Buttton");

        QHBoxLayout *box = new QHBoxLayout();
        box->addWidget(button);
        widget->setLayout(box);
        QTabWidget *tabs = new QTabWidget();
        
        VisualizationWindow = new VisualizationModule;
        RegistrationWindow = new RegistrationModule;

        tabs->addTab( RegistrationWindow,"Registration Window");
        tabs->addTab(VisualizationWindow,"Visualization Window");
        tabs->setMovable(true);

          
        QStatusBar  *statusBar = new QStatusBar();
        statusBar->setStyleSheet("border: 1px ");
        statusBar->showMessage("Ready");
        VB_widowLayout =new QVBoxLayout();
        VB_widowLayout->addWidget(tabs);
        VB_widowLayout->addWidget(statusBar);

        centralWidget->setLayout(VB_widowLayout);

        
        mainwindow->setCentralWidget(centralWidget);
//        mainwindow->setStyleSheet("background:#424949; color: white");
        
        retranslateUi(mainwindow);
        QMetaObject::connectSlotsByName(mainwindow);

    } // setupUi

      // retranslateUi
    void retranslateUi(QMainWindow *mainwindow)
    {
        mainwindow->setWindowTitle(QApplication::translate("Application", "Application", 0, QApplication::UnicodeUTF8));


    }

};

namespace Ui {
    class mainwindow: public Ui_mainwindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_mainwindow_H
