#include "VisualizationModule.h"
#include <QApplication>
#include "itkGPUBoxImageFilter.h"
#include <RegistrationModule.h>
#include <mainwindow.h>
#include <QtCore>
int main(int argc, char *argv[])
{	
    QApplication a(argc, argv);
  
	QFile f("../../Application/qdarkstyle/style.qss");
	if (!f.exists())
	{
		printf("Unable to set stylesheet, file not found\n");
	}
	else
	{
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

    // RegistrationModule R;
    // R.show();

    mainwindow   m;
    m.show();
    return a.exec();
}
