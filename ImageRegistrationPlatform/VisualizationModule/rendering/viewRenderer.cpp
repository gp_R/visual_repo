#include "viewRenderer.h"

viewRenderer::viewRenderer(){}

viewRenderer::~viewRenderer(){

    m_planeWidget[3]      = NULL;
    m_resliceImgViewer[3] = NULL;
    m_reader              = NULL;
    m_volRenderer         = NULL;
    m_ipwRenderer         = NULL;
    m_table               = NULL;

}


void viewRenderer::loadData(char *folderDir){

    qDebug()<<" Loading data from: " << folderDir;

    volumeDir_ = folderDir;
    if(m_reader)
        m_reader = NULL;

    m_reader = vtkSmartPointer<vtkDICOMImageReader>::New();
    m_reader->SetDirectoryName(folderDir);
    m_reader->Update();
    m_reader->GetOutput()->GetDimensions(imageDims);


}

void viewRenderer::renderCT3DView(){

//    qDebug()<< "Rendereing volume!";
//    //Marching Cubes
//    m_skinExtractor = vtkSmartPointer<vtkMarchingCubes>     ::New();
//    m_stripper      = vtkSmartPointer<vtkStripper>          ::New();
//    m_skinMapper    = vtkSmartPointer<vtkPolyDataMapper>    ::New();
//    m_volRenderer      = vtkSmartPointer<vtkRenderer>          ::New();
    m_camera        = vtkSmartPointer<vtkCamera>            ::New();

//    m_skinExtractor->SetInputConnection(m_reader->GetOutputPort());

//    m_stripper->SetInputConnection(m_skinExtractor->GetOutputPort());

//    m_skinMapper->SetInputConnection(m_stripper->GetOutputPort());
//    m_skinMapper->ScalarVisibilityOff(); //Turn off flag to control scalar data is used to color objects.

//    m_skin = vtkActor::New();
//    m_skin->SetMapper(m_skinMapper);

//    m_volRenderer->AddActor(m_skin);

//    m_camera->SetViewUp(0, 0, -1);
//    m_camera->SetPosition(0, 1, 0);
//    m_camera->SetFocalPoint(0, 0, 0);
//    m_camera->ComputeViewPlaneNormal();
//    m_camera->Azimuth(30.0);
//    m_camera->Elevation(30.0);

//    m_volRenderer->SetActiveCamera(m_camera);
//    m_volRenderer->ResetCamera();
    
//    m_CT3DWindow->AddRenderer(m_volRenderer);

//    qDebug()<< "Rendereing volume done!";



    //Ray casting
    m_renderer = new Render3D(m_CT3DWindow);
    m_renderer ->loadData(volumeDir_);
    m_renderer->rescaleData(m_renderer->getScale(),
                           m_renderer->getShift());
    m_renderer->setShifter(m_renderer->getShifter());
    m_renderer->setInputData(m_renderer->
                                      getShifter()->GetOutput());
    m_renderer->rayCastingRendering();

    emit FINISHED();


}


//void viewRenderer::renderCubes(){

//    //  Marching Cube
//    Render3D *renderer = new Render3D(m_CT3DWindow);
//    renderer ->loadData(volumeDir_);
//    renderer->rescaleData(renderer->getScale(),
//                           renderer->getShift());
//    renderer->cubeMarchingExtraction(500, renderer->
//    getShifter()->GetOutput());
//}


void viewRenderer::renderMPR3DView(){

    mMinInt= -3000;
    mMaxInt=  3000;

    for (int i = 0; i < 3; i++)
    {
        m_resliceImgViewer[i] = vtkSmartPointer<vtkResliceImageViewer>::New();
    }

    //set up rendering windows
    m_resliceImgViewer[2]->SetRenderWindow(m_orientSGWindow);
    m_resliceImgViewer[2]->SetupInteractor(m_orientSGWindow->GetInteractor());

    m_resliceImgViewer[1]->SetRenderWindow(m_orientCRWindow);
    m_resliceImgViewer[1]->SetupInteractor(m_orientCRWindow->GetInteractor());

    m_resliceImgViewer[0]->SetRenderWindow(m_orientAXWindow);
    m_resliceImgViewer[0]->SetupInteractor(m_orientAXWindow->GetInteractor());


    // Create a greyscale lookup table
    m_table = vtkSmartPointer<vtkLookupTable>::New();
    m_table->SetRange(-3000, 3000);                       // image intensity range
    m_table->SetValueRange(0.0, 1.0);                     // from black to white
    m_table->SetSaturationRange(0.0, 0.0);                // no color saturation
    // table->SetTableValue(1.0,0.0,0.0,0.0,0.0);
    m_table->SetRampToLinear();
    m_table->Build();

     // make them all share the same reslice cursor object.
    for (int i = 0; i < 3; i++)
    {
      vtkResliceCursorLineRepresentation *rep =
              vtkResliceCursorLineRepresentation::SafeDownCast(
                  m_resliceImgViewer[i]->GetResliceCursorWidget()->GetRepresentation());

      rep->GetResliceCursorActor()->
        GetCursorAlgorithm()->SetReslicePlaneNormal(i);
      rep->SetManipulationMode(vtkResliceCursorRepresentation::RotateBothAxes);

      m_resliceImgViewer[i]->SetResliceCursor(m_resliceImgViewer[0]->GetResliceCursor());
      m_resliceImgViewer[i]->SetInputData(m_reader->GetOutput());
      m_resliceImgViewer[i]->SetLookupTable(m_table);
      m_resliceImgViewer[i]->SetSliceOrientation(i);
      m_resliceImgViewer[i]->SliceScrollOnMouseWheelOn();
      m_resliceImgViewer[i]->SetResliceModeToAxisAligned();
      m_resliceImgViewer[i]->GetImageActor()->RotateY(-180);
      m_resliceImgViewer[i]->GetResliceCursorWidget()->
              GetResliceCursorRepresentation()->RestrictPlaneToVolumeOn();
      m_resliceImgViewer[i]->SetColorLevel(2);
      m_resliceImgViewer[i]->SetSlice(imageDims[i]/2);
      m_resliceImgViewer[i]->GetRenderer()->ResetCamera();
      m_resliceImgViewer[i]->GetRenderer()->GetActiveCamera()->Zoom(1.25);
      m_resliceImgViewer[i]->Render();


    }

    if(m_ipwRenderer)
        m_ipwRenderer = NULL;

    LOG_DEBUG("Initializing Renderer, Interactor");

    m_ipwRenderer = vtkSmartPointer<vtkRenderer>::New();


    vtkSmartPointer<vtkRenderWindowInteractor> iren =
     vtkSmartPointer<vtkRenderWindowInteractor>::New();

    m_MPR3DWindow->AddRenderer(m_ipwRenderer);
    iren = m_MPR3DWindow->GetInteractor();

    vtkSmartPointer<vtkCellPicker> picker =
      vtkSmartPointer<vtkCellPicker>::New();

    picker->SetTolerance(0.005);

    for (int i = 0; i < 3; i++)
    {
      double color[3] = {0, 0, 0};
      color[i] = 1;

      m_planeWidget[i] = vtkSmartPointer<vtkImagePlaneWidget>::New();
      m_planeWidget[i]->SetInteractor(iren);
      m_planeWidget[i]->SetPicker(picker);
      m_planeWidget[i]->RestrictPlaneToVolumeOn();
      m_planeWidget[i]->GetPlaneProperty()->SetColor(color);
      m_planeWidget[i]->SetInputConnection(m_reader->GetOutputPort());
      m_planeWidget[i]->TextureInterpolateOn();
      m_planeWidget[i]->SetResliceInterpolateToLinear();
      m_planeWidget[i]->GetTexturePlaneProperty()->SetOpacity(1.0);
      m_planeWidget[i]->SetLookupTable(m_table);
      m_planeWidget[i]->SetPlaneOrientation(i);
      m_planeWidget[i]->SetSliceIndex(imageDims[i]/2);
      m_planeWidget[i]->DisplayTextOn();
      m_planeWidget[i]->SetMarginSizeX(0);
      m_planeWidget[i]->SetMarginSizeY(0);
      m_planeWidget[i]->On();
      m_planeWidget[i]->InteractionOn();

    }


    vtkSmartPointer<vtkResliceCursorCallback> cbk =
      vtkSmartPointer<vtkResliceCursorCallback>::New();

    vtkSmartPointer<customvtkResliceImageViewerScrollCallback> scroll_cbk =
      vtkSmartPointer<customvtkResliceImageViewerScrollCallback>::New();

      vtkSmartPointer<vtkImagePlaneWidgetCallback> plane_cbk =
        vtkSmartPointer<vtkImagePlaneWidgetCallback>::New();

     //Add observers to interact dynamically through the slices
    for (int i = 0; i < 3; i++)
    {
      cbk->IPW[i] = m_planeWidget[i];
      cbk->RCW[i] = m_resliceImgViewer[i]->GetResliceCursorWidget();

      m_resliceImgViewer[i]->GetResliceCursorWidget()->AddObserver(
          vtkResliceCursorWidget::ResliceAxesChangedEvent, cbk );
      m_resliceImgViewer[i]->GetResliceCursorWidget()->AddObserver(
          vtkResliceCursorWidget::WindowLevelEvent, cbk );
      m_resliceImgViewer[i]->GetResliceCursorWidget()->AddObserver(
          vtkResliceCursorWidget::ResetCursorEvent, cbk );
      m_resliceImgViewer[i]->GetInteractorStyle()->AddObserver(
          vtkCommand::WindowLevelEvent, cbk );

      scroll_cbk->Viewer[i]=m_resliceImgViewer[i];
      scroll_cbk->IPW[i]=m_planeWidget[i];

      plane_cbk->Viewer[i] = m_resliceImgViewer[i];
      plane_cbk->IPW[i]    = m_planeWidget[i];

      m_resliceImgViewer[i]->AddObserver(vtkCommand::InteractionEvent, scroll_cbk);
//      m_resliceImgViewer[i]->AddObserver(vtkCommand::ModifiedEvent, scroll_cbk);

      m_planeWidget[i]->AddObserver(vtkCommand::InteractionEvent,plane_cbk);

      // Make them all share the same color map.
      m_planeWidget[i]->GetColorMap()->SetLookupTable(m_resliceImgViewer[0]->GetLookupTable());
      m_planeWidget[i]->SetColorMap(m_resliceImgViewer[i]->GetResliceCursorWidget()->
                                    GetResliceCursorRepresentation()->GetColorMap());

    }

}


void viewRenderer::renderOrientView(){}


void viewRenderer::setVolumeRenderWindow(vtkRenderWindow *win){

    if(!win || win==NULL){
        LOG_ERRORd("No Render Window");
        return;
    }
    
    m_CT3DWindow = win;
    qDebug()<<"Volume render window is set!";
}

void viewRenderer::setMPR3DRenderWindow(vtkRenderWindow *win){
    m_MPR3DWindow = win;
}

void viewRenderer::setOrientRenderWindows(
    vtkRenderWindow *AXwind,vtkRenderWindow *CRwind,vtkRenderWindow *SGwind){
        
        m_orientAXWindow = AXwind;
        m_orientCRWindow = CRwind;
        m_orientSGWindow = SGwind;

}

int viewRenderer::getImgDims(int i ){
    return imageDims[i];

}

void viewRenderer::updateSat(double value){
    if(m_table){
        for(int i=0; i<3; i++){
          m_table->SetSaturationRange(0.0, value);
          m_table->Build();
          m_resliceImgViewer[i]->Render();
          m_planeWidget[i]->GetInteractor()->GetRenderWindow()->Render();
        }
      }
}


void viewRenderer::updateOpac(double value){

          for(int i=0;i<3; i++){
            if(m_planeWidget[i]){
                m_planeWidget[i]->GetTexturePlaneProperty()->SetOpacity(value);
                m_planeWidget[i]->GetInteractor()->GetRenderWindow()->Render();
                m_resliceImgViewer[i]->GetImageActor()->SetOpacity(value);
                m_resliceImgViewer[i]->Render();
                // m_resliceImgViewer2[i]->GetImageActor()->SetOpacity(value/2.0);
                // m_resliceImgViewer2[i]->Render();

              }
        }

}

void viewRenderer::updateMinInt(int position){
    for(int i=0; i<3; i++){
          mMinInt=position;
          m_resliceImgViewer[i]->GetLookupTable()->SetRange(position,mMaxInt);
          m_resliceImgViewer[i]->Render();
          m_planeWidget[i]->GetInteractor()->GetRenderWindow()->Render();
          m_planeWidget[i]->UpdatePlacement();
        }
   }

void viewRenderer::updateMaxInt(int position){

    //move slider without data falut--on it, condition changing
        for(int i=0; i<3; i++){
                  mMaxInt=position;
                  m_resliceImgViewer[i]->GetLookupTable()->SetRange(mMinInt,position);
                  m_resliceImgViewer[i]->Render();
                  m_planeWidget[i]->GetInteractor()->GetRenderWindow()->Render();
                  m_planeWidget[i]->UpdatePlacement();
                }
      }


void viewRenderer::enableObliqueMode(int mode){

        for (int i = 0; i < 3; i++){
          if(mode){
            m_resliceImgViewer[i]->SetResliceMode(1);
          }
          else{
            m_resliceImgViewer[i]->SetResliceMode(0);

          }
          m_resliceImgViewer[i]->GetRenderer()->ResetCamera();
          m_resliceImgViewer[i]->Render();
      }

}

void viewRenderer::updateSGSlicePosition(int position){

        m_resliceImgViewer[0]->SetSlice(position);
        m_resliceImgViewer[0]->Render();

        m_planeWidget[0]->SetSliceIndex( m_resliceImgViewer[0]->GetImageActor()->GetSliceNumber());
        m_planeWidget[0]->GetInteractor()->GetRenderWindow()->Render();
        m_planeWidget[0]->UpdatePlacement();



}

void viewRenderer::updateCRSlicePosition(int position){
        m_resliceImgViewer[1]->SetSlice(position);
        m_resliceImgViewer[1]->Render();

        m_planeWidget[1]->SetSliceIndex( m_resliceImgViewer[1]->GetImageActor()->GetSliceNumber());
        m_planeWidget[1]->GetInteractor()->GetRenderWindow()->Render();
        m_planeWidget[1]->UpdatePlacement();



}

void viewRenderer::updateAXSlicePosition(int position){
        m_resliceImgViewer[2]->SetSlice(position);
        m_resliceImgViewer[2]->Render();

        m_planeWidget[2]->SetSliceIndex( m_resliceImgViewer[2]->GetImageActor()->GetSliceNumber());
        m_planeWidget[2]->GetInteractor()->GetRenderWindow()->Render();
        m_planeWidget[2]->UpdatePlacement();





}

