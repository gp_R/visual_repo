#ifndef VIEWRENDERER_H
#define VIEWRENDERER_H

#include <QtCore>

//Include VTK headers
#include <vtkSmartPointer.h>
#include <vtkObjectFactory.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkImageViewer2.h>
#include <vtkDICOMImageReader.h>
#include <vtkInteractorStyleImage.h>
#include <vtkActor2D.h>
#include <vtkTextProperty.h>
#include <vtkTextMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkCamera.h>
#include <vtkMarchingCubes.h>
#include <vtkOutlineFilter.h>
#include <vtkRenderWindow.h>
#include <vtkImagePlaneWidget.h>
#include <vtkCellPicker.h>
#include <vtkResliceImageViewer.h>
#include <vtkImageActor.h>
#include <vtkRenderWindow.h>
#include <vtkCommand.h>
#include <vtkVersion.h>
#include <vtkStripper.h>
#include <vtkCallbackCommand.h>
#include <vtkLookupTable.h>
#include <vtkImageMapToColors.h>
#include <vtkImageReslice.h>
#include <vtkPlane.h>
#include <vtkCutter.h>
#include <vtkResliceCursorThickLineRepresentation.h>
#include <vtkResliceCursorWidget.h>
#include <vtkResliceCursorActor.h>
#include <vtkResliceCursorPolyDataAlgorithm.h>
#include <vtkResliceCursor.h>
#include <vtkBoxRepresentation.h>
#include <vtkPlaneSource.h>
#include <vtkBoxWidget2.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPlanes.h>
#include <vtkTransform.h>
#include <vtkImageData.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <DICOMParser.h>


//Include custom classes [utils]
#include "customvtkResliceImageViewerScrollCallback.h"
#include "vtkImagePlaneWidgetCallback.h"
#include "vtkResliceCursorCallback.h"
//#include "vtkBoxCallback.h"

#include "Render3D.h"
#include "Logger.h"

class viewRenderer : public QObject{
Q_OBJECT
public:
    viewRenderer();
    ~viewRenderer();
    void renderMPR3DView();
    void renderOrientView();
    void loadData(char *folderDir);
    
    void setVolumeRenderWindow(vtkRenderWindow *win);
    void setMPR3DRenderWindow(vtkRenderWindow *AXwind);
    void setOrientRenderWindows(vtkRenderWindow *AXwind,vtkRenderWindow *CRwind,vtkRenderWindow *SGwind);

    int getImgDims(int i);

    void updateSat(double value);
    void updateOpac(double value);
    void updateMinInt(int position);
    void updateMaxInt(int position);
    void enableObliqueMode(int mode);
    void updateSGSlicePosition(int position);
    void updateCRSlicePosition(int position);
    void updateAXSlicePosition(int position);
    void renderCubes();









public slots:
    void renderCT3DView();
signals:
    void FINISHED();


private:

int imageDims[3];
int mMinInt;
int mMaxInt;

vtkSmartPointer<vtkDICOMImageReader> m_reader;
vtkSmartPointer<vtkMarchingCubes>    m_skinExtractor;
vtkSmartPointer<vtkPolyDataMapper>   m_skinMapper;
vtkSmartPointer<vtkStripper>         m_stripper;
vtkSmartPointer<vtkActor>            m_skin;
vtkSmartPointer<vtkRenderer>         m_volRenderer;
vtkSmartPointer<vtkCamera>           m_camera;

vtkSmartPointer<vtkResliceImageViewer>     m_resliceImgViewer[3];
vtkSmartPointer<vtkImagePlaneWidget>       m_planeWidget[3];
vtkSmartPointer<vtkRenderer>               m_ipwRenderer;
vtkSmartPointer<vtkLookupTable>            m_table ;


vtkSmartPointer<vtkRenderWindow>   m_CT3DWindow;
vtkSmartPointer<vtkRenderWindow>   m_MPR3DWindow;
vtkSmartPointer<vtkRenderWindow>   m_orientAXWindow;
vtkSmartPointer<vtkRenderWindow>   m_orientCRWindow;
vtkSmartPointer<vtkRenderWindow>   m_orientSGWindow;

Render3D *m_renderer;

char *volumeDir_;

};
#endif //VIEWRENDERER_H
