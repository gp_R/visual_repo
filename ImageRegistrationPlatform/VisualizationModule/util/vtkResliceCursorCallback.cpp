
#include "vtkResliceCursorCallback.h"
#include <vtkObjectFactory.h> //for new() macro

#include <vtkResliceCursorWidget.h>
#include <vtkPlaneSource.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkResliceCursorRepresentation.h>
#include <vtkRenderWindow.h>

vtkStandardNewMacro(vtkResliceCursorCallback);


void vtkResliceCursorCallback::Execute( vtkObject *caller, unsigned long ev, void *callData ){
  if (ev == vtkResliceCursorWidget::WindowLevelEvent ||
      ev == vtkCommand::WindowLevelEvent ||
      ev == vtkResliceCursorWidget::ResliceThicknessChangedEvent)
      {
    // Render everything
    this->renderAll();
  }


  vtkImagePlaneWidget* ipw =
    dynamic_cast< vtkImagePlaneWidget* >( caller );

  if (ipw)
  {
    std::cout << "/* message 2 */" << std::endl;

    double* wl = static_cast<double*>( callData );

    int i;
    if(ipw== this->IPW[0]){
      i=0;
    }
    else if(ipw=this->IPW[1]){
      i=1;
    }
    else if(ipw=this->IPW[2]){
      i=2;
    }

    vtkPlaneSource *ps = static_cast<vtkPlaneSource*> (this->RCW[i]->GetResliceCursorRepresentation()->GetPlaneSource());
    if(ps){
    ps->SetPoint1(this->IPW[i]->GetPoint1());
    ps->SetPoint2(this->IPW[i]->GetPoint2());
    ps->SetOrigin(this->IPW[i]->GetOrigin());
    }

    // Render everything
    this->renderAll();

  }

  vtkResliceCursorWidget *rcw = dynamic_cast<
    vtkResliceCursorWidget * >(caller);
  if (rcw)
  {

  //  vtkResliceCursorLineRepresentation *rep = dynamic_cast<
  //     vtkResliceCursorLineRepresentation * >(rcw->GetRepresentation());
  //   rep->GetResliceCursorActor()->GetCursorAlgorithm()->GetResliceCursor();

    for (int i = 0; i < 3; i++)
    {
      vtkPlaneSource *ps = static_cast< vtkPlaneSource * >(
          this->IPW[i]->GetPolyDataAlgorithm());

      if(ps){
      ps->SetOrigin(this->RCW[i]->GetResliceCursorRepresentation()->
                                        GetPlaneSource()->GetOrigin());

      ps->SetPoint1(this->RCW[i]->GetResliceCursorRepresentation()->
                                        GetPlaneSource()->GetPoint1());
      ps->SetPoint2(this->RCW[i]->GetResliceCursorRepresentation()->
                                        GetPlaneSource()->GetPoint2());
      }

      // If the reslice plane has modified, update it on the 3D widget
      this->IPW[i]->UpdatePlacement();

    }
  }
  // Render everything
  this->renderAll();
}

void vtkResliceCursorCallback::renderAll(){
  for (int i = 0; i < 3; i++)
  {
    this->RCW[i]->Render();
  }
  this->IPW[0]->GetInteractor()->GetRenderWindow()->Render();

}
