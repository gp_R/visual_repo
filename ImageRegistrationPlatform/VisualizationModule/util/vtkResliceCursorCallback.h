#ifndef vtkResliceCursorCallback_H
#define vtkResliceCursorCallback_H

#include <vtkCommand.h>
#include <vtkImagePlaneWidget.h>
#include <vtkResliceImageViewer.h>

class vtkResliceCursorCallback : public vtkCommand
{
public:
  static vtkResliceCursorCallback *New();

    void Execute( vtkObject *caller, unsigned long ev, void *callData );
    void renderAll();

    vtkImagePlaneWidget* IPW[3];
    vtkResliceCursorWidget *RCW[3];


  protected:
    vtkResliceCursorCallback(){}
    ~vtkResliceCursorCallback(){}
  };
#endif
