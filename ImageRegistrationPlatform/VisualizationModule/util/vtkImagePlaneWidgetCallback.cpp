#include "vtkImagePlaneWidgetCallback.h"
#include "vtkObjectFactory.h"

#include <vtkImagePlaneWidget.h>
#include <vtkResliceImageViewer.h>
#include <vtkRenderWindowInteractor.h>

vtkStandardNewMacro(vtkImagePlaneWidgetCallback);

void vtkImagePlaneWidgetCallback::Execute(vtkObject *caller, unsigned long ev, void*)
  {
  // Do not process if any modifiers are ON
  for(int i=0; i<3; i++){
      if (this->IPW[i]->GetInteractor()->GetShiftKey() ||
          this->IPW[i]->GetInteractor()->GetControlKey() ||
          this->IPW[i]->GetInteractor()->GetAltKey())
        {
        return;
        }
      }

      vtkImagePlaneWidget* ipw =
        dynamic_cast< vtkImagePlaneWidget* >( caller );

      if (ipw)
      {
        if(ipw == this->IPW[0]){
          this->Viewer[0]->SetSlice(ipw->GetSliceIndex());
          this->Viewer[0]->Render();
        }
        else if(ipw==this->IPW[1]){
          this->Viewer[1]->SetSlice(ipw->GetSliceIndex());
          this->Viewer[1]->Render();

        }
        else if(ipw==this->IPW[2]){
          this->Viewer[2]->SetSlice(ipw->GetSliceIndex());
          this->Viewer[2]->Render();

        }

      }
  // Abort further event processing for the scroll.
  this->SetAbortFlag(1);
}
