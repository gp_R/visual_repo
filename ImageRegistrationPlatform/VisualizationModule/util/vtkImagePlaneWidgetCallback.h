
#ifndef vtkImagePlaneWidgetCallback_H
#define vtkImagePlaneWidgetCallback_H

#include <vtkCommand.h>
#include <vtkImagePlaneWidget.h>
#include <vtkResliceImageViewer.h>

class vtkImagePlaneWidgetCallback : public vtkCommand
{
public:
  static vtkImagePlaneWidgetCallback *New();
  void Execute(vtkObject *caller, unsigned long ev, void*);


  vtkImagePlaneWidgetCallback() {}
  vtkResliceImageViewer *Viewer[3];
  vtkImagePlaneWidget* IPW[3];

};
#endif
