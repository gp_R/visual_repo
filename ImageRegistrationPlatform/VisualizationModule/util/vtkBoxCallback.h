
class vtkBoxCallback : public vtkCommand
 {
 public:
   static vtkBoxCallback *New()
   {
     return new vtkBoxCallback;
   }

   vtkSmartPointer<vtkActor> m_actor;
   vtkSmartPointer<vtkPolyDataMapper> m_mapper;

   void SetActor( vtkSmartPointer<vtkActor> actor )
   {
     m_actor = actor;
   }

   void SetMapper( vtkSmartPointer<vtkPolyDataMapper> mapper )
   {
     m_mapper = mapper;
   }

   virtual void Execute( vtkObject *caller, unsigned long, void* )
   {
     vtkBoxWidget2 *boxWidget =
 			reinterpret_cast<vtkBoxWidget2*>(caller);

     vtkSmartPointer<vtkTransform> t =
       vtkSmartPointer<vtkTransform>::New();

     vtkSmartPointer<vtkPlanes> planes =
        vtkSmartPointer<vtkPlanes>::New();
     vtkBoxRepresentation::SafeDownCast( boxWidget->GetRepresentation() )->GetTransform( t );
     reinterpret_cast<vtkBoxRepresentation*>(boxWidget->GetRepresentation())->GetPlanes(planes);

     this->m_actor->GetMapper()->SetClippingPlanes(planes);
     this->m_actor->VisibilityOn();
      }

   vtkBoxCallback(){}
 };
