#include "customvtkResliceImageViewerScrollCallback.h"
#include "vtkObjectFactory.h" //for new() macro

vtkStandardNewMacro(customvtkResliceImageViewerScrollCallback);

void customvtkResliceImageViewerScrollCallback::Execute(vtkObject *caller, unsigned long ev, void*)
  {
  // Do ;  if(m_viewer == this->Viewer[0]){
  vtkResliceImageViewer *m_viewer = dynamic_cast<
  vtkResliceImageViewer * >(caller);

   if(m_viewer == this->Viewer[0]){
      this->IPW[0]->SetSliceIndex( m_viewer->GetImageActor()->GetSliceNumber());
      this->IPW[0]->GetInteractor()->GetRenderWindow()->Render();
      this->IPW[0]->UpdatePlacement();

    }
    else if(m_viewer == this->Viewer[1]){
      this->IPW[1]->SetSliceIndex(m_viewer->GetImageActor()->GetSliceNumber());
      this->IPW[1]->GetInteractor()->GetRenderWindow()->Render();
      this->IPW[1]->UpdatePlacement();
    }
    else if(m_viewer == this->Viewer[2]){
      this->IPW[2]->SetSliceIndex( m_viewer->GetImageActor()->GetSliceNumber());
      this->IPW[2]->GetInteractor()->GetRenderWindow()->Render();
      this->IPW[2]->UpdatePlacement();
    }

    for(int i=0; i<3; i++){
        if (this->IPW[i]->GetInteractor()->GetShiftKey() ||
            this->IPW[i]->GetInteractor()->GetControlKey() ||
            this->IPW[i]->GetInteractor()->GetAltKey())
          {
          return;
          }
        }

    vtkImagePlaneWidget* ipw =
      dynamic_cast< vtkImagePlaneWidget* >( caller );

  // Abort further event processing for the scroll.
  this->SetAbortFlag(1);
}
