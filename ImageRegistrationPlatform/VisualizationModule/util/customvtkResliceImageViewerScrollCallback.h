#ifndef customvtkResliceImageViewerScrollCallback_H
#define customvtkResliceImageViewerScrollCallback_H

#include <vtkCommand.h>
#include <vtkResliceImageViewer.h>
#include <vtkImagePlaneWidget.h>
#include <vtkImageActor.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>

class customvtkResliceImageViewerScrollCallback : public vtkCommand
{
public:
  // vtkTypeMacro(customvtkResliceImageViewerScrollCallback,vtkCommand);
  static customvtkResliceImageViewerScrollCallback *New();
  // { return new customvtkResliceImageViewerScrollCallback; }

   void Execute(vtkObject *caller, unsigned long ev, void*);

  vtkResliceImageViewer *Viewer[3];
  vtkImagePlaneWidget* IPW[3];

};
#endif
