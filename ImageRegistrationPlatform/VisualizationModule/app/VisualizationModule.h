#ifndef VISUALIZATIONMODULE_H
#define VISUALIZATIONMODULE_H

#include <sstream>

//Include Qt headers
#include <QMainWindow>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QThread>
#include <QtCore>

#include "viewRenderer.h"
#include "Logger.h"

namespace Ui {

class VisualizationModule;

}


class VisualizationModule : public QMainWindow
{
    Q_OBJECT

public:
    explicit VisualizationModule(QWidget *parent = 0);
    ~VisualizationModule();

public slots:
    void setDirPath(QString volumePath);
    void setRenderer(vtkRenderer *renderer);
    void SetRenderingStartON();

private slots:


    /**
     * @brief U_browse
     */
    void U_browse();

    /**
     * @brief U_renderViews
     */
    void U_renderViews();

    /**
     * @brief U_changeIso
     * @param value
     */
    void U_changeIso(int value);


    /**
     * @brief U_ChangeRendMode
     * @param index
     */
//    void U_ChangeRendMode(int index);

    /**
     * @brief U_ChangeViewMode
     * @param index
     */
    void U_ChangeViewMode(int index);
    /**
     * @brief U_enableObliqueMode
     * @param mode
     */
    void U_enableObliqueMode( int mode);

    /**
     * @brief U_setMinInt
     * @param position
     */
    void U_setMinInt(int position);

    /**
     * @brief U_setMaxInt
     * @param position
     */
    void U_setMaxInt(int position);

    /**
     * @brief U_updateMaxIntSlider
     * @param value
     */
    void U_updateMaxIntSlider(double value);

    /**
     * @brief U_updateMinIntSlider
     * @param value
     */
    void U_updateMinIntSlider(double value);

    /**
     * @brief U_setOpac
     * @param value
     */
    void U_setOpac(double value);

    /**
     * @brief U_setSat
     * @param value
     */
    void U_setSat(double value);


    /**
     * @brief updateRenderWindow
     */
    void updateRenderWindow();

    /**
     * @brief U_updateAXSlicePosition
     * @param position
     */
    void U_updateAXSlicePosition(int position);

    /**
     * @brief U_updateSGSlicePosition
     * @param position
     */
    void U_updateSGSlicePosition(int position);

    /**
     * @brief U_updateCRSlicePosition
     * @param position
     */
    void U_updateCRSlicePosition(int position);


signals:
    /**
     * @brief signalProgress
     * @param value
     */
    void signalProgress(int value);
    void FINISHED();


private:
    Ui::VisualizationModule *ui;

    bool isMPR;
    bool isRendered;
    int imageDims[3];
    int mMinInt;
    int mMaxInt;
    bool isLoaded;


    viewRenderer *m_viewRenderer;
    QThread      *renderThread;

    QString m_volumePath;

    Render3D *rendererCubes;


    /**
     * @brief emitProgress
     */
    void emitProgress(int value);

    /**
     * @brief setupWidgets
     */
    void setupWidgets();



};

#endif //VISUALIZATIONMODULE_H
