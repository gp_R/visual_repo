
#ifndef UI_VisualizationModule_H
#define UI_VisualizationModule_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QWidget>
#include <QtGui/QCheckBox>
#include <QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QGroupBox>
#include <QtGui/QDoubleSpinBox>
#include <QTextEdit>
#include <QSize>
#include <QTabWidget>
#include "QVTKWidget.h"
#include <QSplitter>
#include <QFrame>
#include <QProgressBar>
#include <QComboBox>


QT_BEGIN_NAMESPACE



class Ui_VisualozationWindow
{

public:
    QWidget *centralWidget;

    QVTKWidget     *MPR3DView;
    QVTKWidget     *sagittalView;
    QVTKWidget     *axialView;
    QVTKWidget     *coronalView;
    QVTKWidget     *volumeView;

    QHBoxLayout    *HB_3DViews;
    QLabel         *AXlabel;
    QLabel         *SGlabel;
    QLabel         *CRlabel;

    QSlider        *AXSlider;
    QSlider        *SGSlider;
    QSlider        *CRSlider;


    QWidget        *MPRContainer1;
    QWidget        *volContainer1;
    QComboBox      *CB_volModes;
   QComboBox       *CB_rendModes;

    QSplitter  *mainSplitter;
    QSplitter  *viewSplitter;

    QHBoxLayout    *HB_buttons;
    QVBoxLayout    *VB_controls;

    QHBoxLayout    *hboxS;

    QVBoxLayout    *sliderBox;
    QVBoxLayout    *vbox;
    QTextEdit      *text;
    QPushButton    *browseButton;
    QPushButton    *resetButton;
    QCheckBox      *check_volume;
    QCheckBox      *check_Oblique;
    QCheckBox      *check_AxisAligned;
    QCheckBox      *check_planes;
    QSlider        *minInt_Slider;
    QSlider        *maxInt_Slider;
    QLabel         *minInt_label;
    QLabel         *maxInt_label;
    QGroupBox      *Display_GroupBox;
    QHBoxLayout    *minInt_HBox;
    QHBoxLayout    *maxInt_HBox;
    QLabel         *opac_label;
    QHBoxLayout    *colorProp_HBox;
    QHBoxLayout    *trans_HBox;
    QDoubleSpinBox *minInt_SpinBox;
    QDoubleSpinBox *maxInt_SpinBox;
    QDoubleSpinBox *opac_SpinBox;
    QDoubleSpinBox *sat_SpinBox;
    QLabel         *sat_label;

    QProgressBar   *progressbar;

    QHBoxLayout    *HB_windowLayout;
    QVBoxLayout    *VB_volView;
    QVBoxLayout    *VB_MPRviews;
    QHBoxLayout    *HB_orientView;

    QSlider        *SL_changeIso;

    void setupUi(QMainWindow *VisualizationModule)
    {
        if (VisualizationModule->objectName().isEmpty())
        {
            VisualizationModule->setObjectName(QString::fromUtf8("VisualizationModule"));
        }


        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(VisualizationModule->sizePolicy().hasHeightForWidth());
        VisualizationModule->setSizePolicy(sizePolicy);
        
        centralWidget = new QWidget(VisualizationModule);

        minInt_Slider = new QSlider();
        minInt_Slider->setOrientation(Qt::Horizontal);
        minInt_Slider->setRange(-3000,3000);
        minInt_Slider->setSliderPosition(-3000);

        maxInt_Slider = new QSlider();
        maxInt_Slider->setOrientation(Qt::Horizontal);
        maxInt_Slider->setRange(-3000,3000);
        maxInt_Slider->setSliderPosition(3000);


        SL_changeIso  = new QSlider();
        SL_changeIso->setOrientation(Qt::Horizontal);
        SL_changeIso->setRange(0,1500);
        SL_changeIso->setSliderPosition(500);
        SL_changeIso->setEnabled(false);

        QLabel *LB_iso = new QLabel("Denisty");

        QHBoxLayout *HB_iso =new QHBoxLayout();

        HB_iso->addWidget(LB_iso);
        HB_iso->addWidget(SL_changeIso);


        minInt_SpinBox = new QDoubleSpinBox();
        maxInt_SpinBox = new QDoubleSpinBox();

        minInt_SpinBox->setRange(-3000.0, 3000.0);
        minInt_SpinBox->setValue(-3000.0);

        maxInt_SpinBox->setRange(-3000.0, 3000.0);
        maxInt_SpinBox->setValue(3000.0);

        minInt_label = new QLabel();
        maxInt_label = new QLabel();

        minInt_HBox = new QHBoxLayout();
        minInt_HBox->addWidget(minInt_label);
        minInt_HBox->addWidget(minInt_Slider);
        minInt_HBox->addWidget(minInt_SpinBox);

        maxInt_HBox = new QHBoxLayout();
        maxInt_HBox->addWidget(maxInt_label);
        maxInt_HBox->addWidget(maxInt_Slider);
        maxInt_HBox->addWidget(maxInt_SpinBox);

        opac_label   = new QLabel();
        sat_label    = new QLabel();

        opac_SpinBox = new QDoubleSpinBox();
        opac_SpinBox->setRange(0.0,1.0);
        opac_SpinBox->setValue(1.0);
        opac_SpinBox->setSingleStep(0.1);

        sat_SpinBox  = new QDoubleSpinBox();
        sat_SpinBox->setRange(0.0,100.0);
        sat_SpinBox->setValue(0.0);
        sat_SpinBox->setSingleStep(0.1);

        colorProp_HBox = new QHBoxLayout();
        colorProp_HBox->addWidget(opac_label);
        colorProp_HBox->addWidget(opac_SpinBox);
        colorProp_HBox->addWidget(sat_label);
        colorProp_HBox->addWidget(sat_SpinBox);




        QFrame* line44 = new QFrame();
        line44->setFrameShape(QFrame::HLine);
        line44->setFrameShadow(QFrame::Sunken);


        sliderBox = new QVBoxLayout();
        sliderBox->addLayout(HB_iso);
        sliderBox->addWidget(line44);
        sliderBox->addLayout(minInt_HBox);
        sliderBox->addLayout(maxInt_HBox);        
        sliderBox->addLayout(colorProp_HBox);


        Display_GroupBox = new QGroupBox("ToolBox");
        Display_GroupBox->setLayout(sliderBox);

        check_Oblique  = new QCheckBox();
        check_volume   = new QCheckBox();
        check_Oblique->setEnabled(false);
        check_volume->setEnabled(false);

        hboxS = new QHBoxLayout();
        hboxS->addStretch();
        hboxS->addWidget(check_volume);
        hboxS->addStretch();
        hboxS->addWidget(check_Oblique);
        hboxS->addStretch();


        minInt_Slider->setEnabled(0);
        maxInt_Slider->setEnabled(0);
        minInt_SpinBox->setEnabled(0);
        maxInt_SpinBox->setEnabled(0);
        opac_SpinBox->setEnabled(0);
        sat_SpinBox->setEnabled(0);


        browseButton  = new QPushButton();
        resetButton   = new QPushButton();
        HB_buttons    = new QHBoxLayout();

        HB_buttons->addWidget(browseButton);
        HB_buttons->addWidget(resetButton);

        progressbar   = new QProgressBar();
        progressbar->setRange(0,100);
        progressbar->hide();

        CB_volModes  = new QComboBox();
        CB_volModes->addItem("Volume Only");
        CB_volModes->addItem("MPR Only");
        CB_volModes->addItem("Both");
        CB_volModes->setCurrentIndex(1);
        CB_volModes->setEnabled(false);

        QLabel       *labelModes   = new QLabel("View Mode");
        QHBoxLayout  *HB_volModes = new QHBoxLayout();

        HB_volModes->addWidget(labelModes,1);
        HB_volModes->addWidget(CB_volModes,3);


        CB_rendModes  = new QComboBox();
        CB_rendModes->addItem("Ray Casting");
        CB_rendModes->addItem("Marching Cube");
        CB_rendModes->setCurrentIndex(0);
        CB_rendModes->setEnabled(false);


        QLabel       *rendModes   = new QLabel("Render");
        QHBoxLayout  *HB_rendModes = new QHBoxLayout();

        HB_rendModes->addWidget(rendModes,1);
        HB_rendModes->addWidget(CB_rendModes,3);


        VB_controls = new QVBoxLayout();
        VB_controls->addStretch();
        VB_controls->addLayout(HB_buttons);
//        VB_controls->addLayout(HB_rendModes);
        VB_controls->addLayout(HB_volModes);
        VB_controls->addWidget(Display_GroupBox);
        VB_controls->addStretch();
        VB_controls->addWidget(progressbar,Qt::BottomSection);


        MPR3DView    = new QVTKWidget();
        sagittalView = new QVTKWidget();
        axialView    = new QVTKWidget();
        coronalView  = new QVTKWidget();
        volumeView   = new QVTKWidget();

        AXlabel      = new QLabel("Axial");
        CRlabel      = new QLabel("Coronal");
        SGlabel      = new QLabel("Sagittal");

        AXlabel->setAlignment(Qt::AlignCenter);
        AXlabel->setStyleSheet("font-size:18px; border: 0.5px solid blue");
        CRlabel->setAlignment(Qt::AlignCenter);
        CRlabel->setStyleSheet("font-size:18px; border: 0.5px solid green");
        SGlabel->setAlignment(Qt::AlignCenter);
        SGlabel->setStyleSheet("font-size:18px; border: 0.5px solid red");

        QHBoxLayout *HB_labels  = new QHBoxLayout();

        HB_labels->addWidget(AXlabel);
        HB_labels->addWidget(CRlabel);
        HB_labels->addWidget(SGlabel);

        AXSlider         = new QSlider(Qt::Horizontal);
        CRSlider         = new QSlider(Qt::Horizontal);
        SGSlider         = new QSlider(Qt::Horizontal);

        AXSlider->setEnabled(0);
        CRSlider->setEnabled(0);
        SGSlider->setEnabled(0);

        QVBoxLayout   *VB_axial      =  new QVBoxLayout();
        QVBoxLayout   *VB_coronal    =  new QVBoxLayout();
        QVBoxLayout   *VB_sagittal   =  new QVBoxLayout();

        VB_axial->addWidget(AXlabel);
        VB_axial->addWidget(axialView);
        VB_axial->addWidget(AXSlider);

        VB_coronal->addWidget(CRlabel);
        VB_coronal->addWidget(coronalView);
        VB_coronal->addWidget(CRSlider);

        VB_sagittal->addWidget(SGlabel);
        VB_sagittal->addWidget(sagittalView);
        VB_sagittal->addWidget(SGSlider);


        VB_MPRviews      = new QVBoxLayout();  // Orientaions vertical view
        HB_orientView    = new QHBoxLayout();  // Orientaions horizontal view

        HB_orientView->addLayout(VB_axial);
        HB_orientView->addLayout(VB_sagittal);
        HB_orientView->addLayout(VB_coronal);


        QFrame* line1 = new QFrame();
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);
        QFrame* line2 = new QFrame();
        line2->setFrameShape(QFrame::HLine);
        line2->setFrameShadow(QFrame::Sunken);

        QFrame* line3 = new QFrame();
        line3->setFrameShape(QFrame::HLine);
        line3->setFrameShadow(QFrame::Sunken);

        HB_3DViews = new QHBoxLayout();
        HB_3DViews->addWidget(MPR3DView);
        HB_3DViews->addWidget(volumeView);
        volumeView->hide();
        VB_MPRviews->addLayout(HB_orientView);
        VB_MPRviews->addWidget(line3);

        viewSplitter = new QSplitter(Qt::Vertical);

        MPRContainer1 = new QWidget();
        MPRContainer1->setLayout(VB_MPRviews);
        volContainer1 = new QWidget();
        volContainer1->setLayout(HB_3DViews);

        viewSplitter->addWidget(volContainer1);
        viewSplitter->addWidget(MPRContainer1);
        viewSplitter->setStretchFactor(0,5);
        viewSplitter->setStretchFactor(1,1);
        viewSplitter->setCollapsible(0,true);
        viewSplitter->setCollapsible(1,true);
        viewSplitter->setStyleSheet("QSplitter::handle{background:#424949; height:1.5px;}");



        mainSplitter = new QSplitter(Qt::Horizontal);

        QWidget *controlrContainer = new QWidget();
        controlrContainer->setLayout(VB_controls);

        mainSplitter->addWidget(controlrContainer);
        mainSplitter->addWidget(viewSplitter);
        mainSplitter->setStretchFactor(1,1);
        mainSplitter->setStretchFactor(2,4);
        mainSplitter->setCollapsible(0,true);
        mainSplitter->setCollapsible(1,false);
        mainSplitter->scroll(5,5);

        mainSplitter->setStyleSheet("QSplitter::handle{background:#424949; height:1.5px;}");


        HB_windowLayout   = new QHBoxLayout();      // main horizontal layout


        HB_windowLayout->addWidget(mainSplitter);

        centralWidget->setLayout(HB_windowLayout);


        VisualizationModule->setCentralWidget(centralWidget);
        retranslateUi(VisualizationModule);
        QMetaObject::connectSlotsByName(VisualizationModule);

    } // setupUi

      // retranslateUi
    void retranslateUi(QMainWindow *VisualizationModule)
    {
        VisualizationModule->setWindowTitle(QApplication::translate("VisualizationModule", "VisualizationModule", 0, QApplication::UnicodeUTF8));
        browseButton->setText(QApplication::translate("VisualizationModule", "Visualize", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("VisualizationModule", "Browse", 0, QApplication::UnicodeUTF8));

        check_Oblique->setText(QApplication::translate("VisualizationModule", "Oblique Mode", 0, QApplication::UnicodeUTF8));
        check_volume->setText(QApplication::translate("VisualizationModule", "Volume", 0, QApplication::UnicodeUTF8));

        minInt_label->setText(QApplication::translate("VisualizationModule", "Min. Intensity", 0, QApplication::UnicodeUTF8));
        maxInt_label->setText(QApplication::translate("VisualizationModule", "Max. Intensity", 0, QApplication::UnicodeUTF8));
        opac_label->setText(QApplication::translate("VisualizationModule", "Opacity", 0, QApplication::UnicodeUTF8));
        sat_label->setText(QApplication::translate("VisualizationModule", "Saturation", 0, QApplication::UnicodeUTF8));

    }

};




namespace Ui {
    class VisualizationModule: public Ui_VisualozationWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VisualizationModule_H
