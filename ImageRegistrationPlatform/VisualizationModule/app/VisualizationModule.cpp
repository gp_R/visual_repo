#include "VisualizationModule.h"
#include "ui_VisualizationModule.h"
#include "itkGPUContextManager.h"

#include <tiffDisplay.h>
#include <vtkTIFFReader.h>
#include <vtkImageSlice.h>
#include <vtkImageResliceMapper.h>
#include <vtkImageStack.h>
#include <vtkImageProperty.h>
#include <vtkImageReader2.h>
#include <vtkTransform.h>



using namespace std;


VisualizationModule::VisualizationModule(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VisualizationModule)
 {
    ui->setupUi(this);

    //Initialize QVTKWidget render windwos
    this->updateRenderWindow();

    connect(this->ui->browseButton,   SIGNAL(clicked()),            this, SLOT(U_renderViews()));
    connect(this->ui->resetButton,    SIGNAL(clicked()),            this, SLOT(U_browse()));

    connect(this->ui->SL_changeIso,  SIGNAL(sliderMoved(int)),     this, SLOT(U_changeIso(int)));

    connect(this, SIGNAL(signalProgress(int) ), this->ui->progressbar, SLOT( setValue(int) ) );

    connect(this->ui->CB_volModes , SIGNAL(currentIndexChanged(int)),this,SLOT(U_ChangeViewMode(int)));
//    connect(this->ui->CB_rendModes , SIGNAL(currentIndexChanged(int)),this,SLOT(U_ChangeRendMode(int)));

    connect(this->ui->viewSplitter,   SIGNAL(splitterMoved(int,int)), this, SLOT(updateRenderWindow()));

    connect(this->ui->AXSlider,  SIGNAL(sliderMoved(int)),     this, SLOT(U_updateAXSlicePosition(int)));
    connect(this->ui->SGSlider,  SIGNAL(sliderMoved(int)),     this, SLOT(U_updateSGSlicePosition(int)));
    connect(this->ui->CRSlider,  SIGNAL(sliderMoved(int)),     this, SLOT(U_updateCRSlicePosition(int)));


    connect(this->ui->check_Oblique,  SIGNAL(stateChanged(int)),    this, SLOT(U_enableObliqueMode(int)));
    connect(this->ui->minInt_Slider,  SIGNAL(sliderMoved(int)),     this, SLOT(U_setMinInt(int)));
    connect(this->ui->maxInt_Slider,  SIGNAL(sliderMoved(int)),     this, SLOT(U_setMaxInt(int)));
    connect(this->ui->minInt_SpinBox, SIGNAL(valueChanged(double)), this, SLOT(U_updateMinIntSlider(double)));
    connect(this->ui->maxInt_SpinBox, SIGNAL(valueChanged(double)), this, SLOT(U_updateMaxIntSlider(double)));
    connect(this->ui->opac_SpinBox,   SIGNAL(valueChanged(double)), this, SLOT(U_setOpac(double)));
    connect(this->ui->sat_SpinBox,    SIGNAL(valueChanged(double)), this, SLOT(U_setSat(double)));

    //---- Render thread setup
    renderThread = new QThread();
    m_viewRenderer = new viewRenderer();
    m_viewRenderer->moveToThread(renderThread);
    connect(renderThread, SIGNAL(started()), m_viewRenderer, SLOT(renderCT3DView()));
    connect(m_viewRenderer, SIGNAL(FINISHED()), renderThread, SLOT(quit()));

    isMPR = true;
    isRendered = false;
    isLoaded = false;
    mMinInt = -3000;
    mMaxInt =  3000;

}

VisualizationModule::~VisualizationModule()
{

    delete ui;
}


void VisualizationModule::setDirPath(QString volumePath){

    qDebug()<< " Volume passed from reg successfully" << volumePath;

    m_volumePath = volumePath;
    // U_renderViews();
    isLoaded = true;

}


void VisualizationModule::SetRenderingStartON(){

    while (isLoaded){
        this->U_renderViews();
        isLoaded =false;
    }
    emit FINISHED();
}

void VisualizationModule::setRenderer(vtkRenderer *renderer){

    qDebug()<< " Renderer passed from reg successfully";
    ui->volumeView->GetRenderWindow()->AddRenderer(renderer);
    ui->volumeView->GetRenderWindow()->Render();

}

void VisualizationModule::setupWidgets()
{
    imageDims[0] = m_viewRenderer->getImgDims(0);
    imageDims[1] = m_viewRenderer->getImgDims(1);
    imageDims[2] = m_viewRenderer->getImgDims(2);

    qDebug() << "img dime "<<imageDims[0]<<  "   "  <<imageDims[1] << "    " << imageDims[2]  ;

    ui->SGSlider->setRange(0,imageDims[0]);
    ui->CRSlider->setRange(0,imageDims[1]);
    ui->AXSlider->setRange(0,imageDims[2]);

    ui->SGSlider->setValue(imageDims[0]/2);
    ui->CRSlider->setValue(imageDims[1]/2);
    ui->AXSlider->setValue(imageDims[2]/2);


    if(isRendered)
    {
        ui->AXSlider->setEnabled(1);
        ui->CRSlider->setEnabled(1);
        ui->SGSlider->setEnabled(1);
        ui->minInt_Slider->setEnabled(1);
        ui->maxInt_Slider->setEnabled(1);
        ui->minInt_SpinBox->setEnabled(1);
        ui->maxInt_SpinBox->setEnabled(1);
        ui->opac_SpinBox->setEnabled(1);
        ui->sat_SpinBox->setEnabled(1);
        ui->SL_changeIso->setEnabled(1);


    }

}


/*----------- UI Calls and Slots--------------*/


void VisualizationModule::U_browse(){

    if(!m_volumePath.isEmpty() ){
        if(QMessageBox::Yes == QMessageBox::question(this, "You'll kill someone now,be sure", "Sure?",
                                        QMessageBox::Yes|QMessageBox::No)){

        m_volumePath = QFileDialog::getExistingDirectory(this,tr("Load DICOM Data"),
                                                  QDir::currentPath(),QFileDialog::ShowDirsOnly);
        }
        else
            QMessageBox::critical(
                this,
                tr("Rendering"),
                tr("That kind soul!, thanks"));
    }
    else{

        m_volumePath = QFileDialog::getExistingDirectory(this,tr("Load DICOM Data"),
                                                  QDir::currentPath(),QFileDialog::ShowDirsOnly);
    }


}

// Render CT volume   :: Visualize Button
void VisualizationModule::U_renderViews(){

//    QString m_volumePath = QFileDialog::getExistingDirectory(this,tr("Load DICOM Data"),
//                                     QDir::currentPath(),QFileDialog::ShowDirsOnly);

    if(!m_volumePath.isEmpty()&& !m_volumePath.isNull())
    {
        if(renderThread->isRunning()){
            qDebug()<< " render thread is running";
                renderThread->quit();
                renderThread->wait(100);
         }

        qDebug()<< "check poit 1";

        m_viewRenderer->loadData((char*) m_volumePath.toStdString().c_str());

        m_viewRenderer->setMPR3DRenderWindow(this->ui->MPR3DView->GetRenderWindow());

        m_viewRenderer->setVolumeRenderWindow(ui->volumeView->GetRenderWindow());

        qDebug()<< "check poit 2";


//        renderThread->start();

        rendererCubes = new Render3D(this->ui->volumeView->GetRenderWindow());
        rendererCubes ->loadData((char*) m_volumePath.toStdString().c_str());
        rendererCubes->rescaleData(rendererCubes->getScale(),
                               rendererCubes->getShift());
        rendererCubes->cubeMarchingExtraction(500, rendererCubes->
        getShifter()->GetOutput());



        qDebug()<< "check poit 3";

        m_viewRenderer->setOrientRenderWindows(this->ui->sagittalView->GetRenderWindow(),
                            this->ui->coronalView->GetRenderWindow(),this->ui->axialView->GetRenderWindow());
        m_viewRenderer->renderMPR3DView();

        this->ui->CB_volModes->setEnabled(true);
        this->ui->SL_changeIso->setEnabled(true);
//        this->ui->CB_rendModes->setEnabled(true);

        isRendered = true;

        this->setupWidgets();

    }
    else
        QMessageBox::critical(
            this,
            tr("NO Directory Selected"),
            tr("Throw something GOOD to be rendered, please!") );

}


void VisualizationModule::U_changeIso(int value){
        rendererCubes->changeIso(value);
}

//void VisualizationModule::U_ChangeRendMode(int index){
//    switch (index) {
//    case 0: /*volume only*/
//        renderThread->start();
//        break;
//    case 1: /*MPR only*/
//          m_viewRenderer->renderCubes();
//        break;
//    default:
//        break;
//}
//}

// Change MPR layout  :: Layout Button
void VisualizationModule::U_ChangeViewMode(int index){    
    switch (index) {
    case 0: /*volume only*/
        this->ui->MPR3DView->hide();
        this->ui->volumeView->show();
        this->ui->MPRContainer1->hide();
        break;
    case 1: /*MPR only*/
        this->ui->volumeView->hide();
        this->ui->MPR3DView->show();
        if(!this->ui->MPRContainer1->isVisible())
             this->ui->MPRContainer1->show();
        break;
    case 2: /*both views*/
        this->ui->volumeView->show();
        this->ui->MPR3DView->show();
        if(!this->ui->MPRContainer1->isVisible())
             this->ui->MPRContainer1->show();
        break;

    default:
        break;
    }
   }

void VisualizationModule::updateRenderWindow(){

    if(!isRendered)
    {
        vtkRenderWindow *wind   = vtkRenderWindow::New();
        ui->MPR3DView->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->sagittalView->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->axialView->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->coronalView->SetRenderWindow(wind);
        wind = NULL;

        wind = vtkRenderWindow::New();
        ui->volumeView->SetRenderWindow(wind);

        wind->Delete();
        wind = NULL;
    }

}

void VisualizationModule::U_setMinInt(int position){
    ui->minInt_SpinBox->setValue(position);
    m_viewRenderer->updateMinInt(position);

}
void VisualizationModule::U_setMaxInt(int position){
    ui->maxInt_SpinBox->setValue(position);
    m_viewRenderer->updateMaxInt(position);

}

void VisualizationModule::U_updateMaxIntSlider(double value){
    ui->maxInt_Slider->setValue(value);
    if(value > mMinInt){
        ui->maxInt_SpinBox->setValue(value);
        m_viewRenderer->updateMaxInt((int)value);
    }

 }

void VisualizationModule::U_updateMinIntSlider(double value){
    ui->minInt_Slider->setValue(value);
    if(value<mMaxInt){
        ui->minInt_SpinBox->setValue(value);
        m_viewRenderer->updateMinInt((int)value);
    }
}

// Control Opacity value :: Opacity Slider
void VisualizationModule::U_setOpac(double value){
    m_viewRenderer->updateOpac(value);
}

// Control Saturation value :: Opacity Slider
void VisualizationModule::U_setSat(double value){
    m_viewRenderer->updateSat(value);

}

/*--- Visualization Modes ---*/

//Enable/diable oblique mode  :: Oblique Mode Check Box
void VisualizationModule::U_enableObliqueMode(int mode){
    if(!mode)
        this->ui->check_Oblique->setChecked(false);
    m_viewRenderer->enableObliqueMode(mode);

}

void VisualizationModule::U_updateSGSlicePosition(int position)
{
    m_viewRenderer->updateSGSlicePosition(position);


}

void VisualizationModule::U_updateCRSlicePosition(int position)
{
    m_viewRenderer->updateCRSlicePosition(position);
}

void VisualizationModule::U_updateAXSlicePosition(int position)
{
    m_viewRenderer->updateAXSlicePosition(position);

}

void VisualizationModule::emitProgress(int value)
{
	//signal 
    emit signalProgress(value);
}
